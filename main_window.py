'''
author: Team 16
'''

import json
import os
import sys
import threading
import time
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from apscheduler.schedulers.blocking import BlockingScheduler
from UI.UI_designer.main import *
from UI.src.email_login import EmailLoginWindow
from UI.src.email_window import EmailWindow
from UI.src.form_logic.Url_json_list import url_and_json_info
from UI.src.home_window import HomeWindow
from UI.src.login_window import LoginWindow
from UI.src.moodle_window import MoodleWindow
from UI.src.pop_window import Pop_Window
from UI.src.setting_window import SettingWindow
from UI.util.common_util import *

"""
@desc: This is the main window
"""


class MainWindow(QMainWindow, Ui_MainWindow):
    '''
    This is the main window
    '''
    signal = pyqtSignal(list)

    def __init__(self, parent=None):
        '''
        Set the init param of the main window
        :param parent: None
        Read file and build ui
        '''
        super(MainWindow, self).__init__(parent)

        # print(os.getcwd())
        # os.chdir('../')
        # print(os.getcwd())
        url_and_json_info.root = os.getcwd().replace('\\', '/')
        url_and_json_info.root_os = os.getcwd()
        url_and_json_info.download_path = os.path.join(os.getcwd(), 'UserFiles')
        # os.chdir('./src')
        self.username = ''
        self.userpassword = ''
        self.isuserlogin = False  # = self.isloginmoodle
        self.isloginEmail = False
        # setting
        self.setupUi(self)
        self.init_slot()
        self.init_ui()
        self.init_timer(url_and_json_info.update_time, url_and_json_info.monitor_interval)
        self.moodle_window = None

    def job(self):
        '''
        Timed task
        :return: None
        Depend on the time and interval to update
        '''
        ls = url_and_json_info.update_time.split(':')
        d1 = 60 * int(ls[0]) + int(ls[1])
        # print((ls, d1))
        ls = time.strftime("%H:%M", time.localtime(int(time.time()))).split(':')
        d2 = 60 * int(ls[0]) + int(ls[1])
        # print((ls, d2))
        t = int(float(url_and_json_info.monitor_interval) * 60)
        # print(t)
        if t <= 0:
            t = 60
        if (d1 - d2) % t == 0:
            datas = []
            flag = False
            if self.isuserlogin:
                control = url_and_json_info.control
                updata = control.scanning()
                self.moodle_window.auto_downfile()
                tag = 0
                for courseInfo in updata:
                    if len(courseInfo.get('folder')):
                        tag = 1
                        break
                    elif len(courseInfo.get('inner_link')):
                        tag = 1
                        break
                    elif len(courseInfo.get('forum')):
                        tag = 1
                        break

                # self.moodle_window.auto_downfile()
                if tag == 1:
                    flag = True
                    ls = []
                    for item in updata:
                        if_exist = False
                        if len(item.get('folder')):
                            if_exist = True
                            ls.append("Course name: " + str(item.get('Name')))
                            ls.append("Folder: ")
                            for folder in item.get('folder'):
                                ls.append(" " + folder.get('folder_name'))
                        if len(item.get('inner_link')):
                            if not if_exist:
                                ls.append("Course name: " + str(item.get('Name')))
                                if_exist = True
                            ls.append("Inner_link: ")
                            for inner_link in item.get('inner_link'):
                                ls.append(" " + inner_link.get('inner_link_name'))
                        if len(item.get('forum')):
                            title = []
                            if not if_exist:
                                title.append("Course name: " + str(item.get('Name')))
                            title.append("Forum: ")
                            if item.get('forum'):
                                kk = []
                                temp = False
                                for forum in item.get('forum'):
                                    try:
                                        data = forum.get('forum_data')
                                    except:
                                        data = []
                                    if (not data):
                                        data = []
                                    if len(data):
                                        temp = True
                                    else:
                                        continue
                                    kk.append(forum.get('forum_name'))
                                    for discuss in data:
                                        kk.append("  " + discuss.get('discuss_title'))
                                if (if_exist == False) and (temp == False):
                                    flag = False
                                else:
                                    for k in title:
                                        ls.append(k)
                                    for k in kk:
                                        ls.append(k)
                datas.append(ls)
            if self.isloginEmail:
                mails = url_and_json_info.server.get_mails()
                for mail in mails:
                    if int(time.mktime(mail['date'].timetuple())) > url_and_json_info.last_renew_time_email:
                        ls = []
                        f_k = False
                        f_c = False
                        for keyword in url_and_json_info.email_keywords:
                            if keyword in mail['subject'] or keyword in "".join(mail['content_text']):
                                f_k = True
                                break
                        for contact in url_and_json_info.email_contacts:
                            if contact in mail['from']:
                                f_c = True
                                break
                        ls.append('mail')
                        if f_k:
                            ls.append(mail['subject'] + 'keywords!')
                        if f_c:
                            ls.append(mail['subject'] + 'important contacts!')
                        if not f_k and not f_c:
                            ls.append(mail['subject'])
                        datas.append(ls)
                        flag = True
            if not flag:
                ls = []
                ls.append('info')
                ls.append('no updates')
                datas.append(ls)
            print("Datas = " + str(datas))
            self.signal.emit(datas)

    def show_window(self, datas):
        '''
        Create a pop window to show updated information
        :param datas: Messages want to update
        :return: None
        '''
        print('Real-time pop window')
        self.popw = Pop_Window(datas)
        self.popw.show()

    def sche(self):
        '''
        Create a timed task and start it
        :return: None
        '''
        self.scheduler = BlockingScheduler()
        self.scheduler.add_job(self.job, 'interval', seconds=60)
        self.scheduler.start()

    def init_timer(self, update_time, interval):
        '''
        Use thread to update
        :param update_time: time of update
        :param interval: update interval
        :return: None
        '''
        self.signal.connect(self.show_window)
        timer_sche = threading.Thread(target=self.sche)
        # Specify the task to be performed by the child thread through target.You can specify arguments to test1 with args= tuple
        timer_sche.start()

    def init_ui(self):
        '''
        Set init ui and set icon
        :return: None
        '''
        # set WMR label size
        WMR_text = self.main_label_WMR
        WMR_text_pixmap = QPixmap(WMR_ICON)
        WMR_text.setPixmap(WMR_text_pixmap)
        WMR_text.setScaledContents(True)
        # set linedit icon
        # search_action = QAction(self)
        # search_action.setIcon(QIcon(SEARCH_ICON))
        # search_action.triggered.connect(self.search)
        # self.main_edit_search.addAction(search_action, QLineEdit.TrailingPosition)
        # set WMR icon
        WMR_label = self.main_label_icon
        WMR_pixmap = QPixmap(APP_ICON)
        WMR_label.setPixmap(WMR_pixmap)
        WMR_label.setScaledContents(True)
        # set left listWidget
        # Star icon in the left side
        data = '[{"name":"Homepage", "icon":"' + url_and_json_info.root + '/UI/res/img/main_star.png"},' \
                                                                          '{"name":"Moodle", "icon":"' + url_and_json_info.root + '/UI/res/img/main_star.png"},' \
                                                                                                                                  '{"name":"Email", "icon":"' + url_and_json_info.root + '/UI/res/img/main_star.png"},' \
                                                                                                                                                                                         '{"name":"Setting", "icon":"' + url_and_json_info.root + '/UI/res/img/main_star.png"}]'

        # print(data)
        listWidget_data = json.loads(data)
        self.listWidget.setProperty('class', 'Normal')
        self.listWidget.setCurrentRow(0)

        def get_item_widget(data):
            '''
            Set the size and icon of each item in the list widget
            :param data: icon information
            :return: None
            '''
            name = data['name']
            icon = data['icon']
            wight = QWidget()
            layout_main = QHBoxLayout()
            map_l = QLabel()  # icon display
            map_l.setFixedSize(50, 50)
            maps = QPixmap(icon).scaled(50, 50)
            map_l.setPixmap(maps)
            item_name = QLabel(name)
            item_name.setStyleSheet("QLabel{color:rgb(0,0,205);font-size:18px;font-weight:normal;font-family:Arial;}")
            item_name.setAlignment(Qt.AlignCenter)
            layout_main.addWidget(map_l)
            layout_main.addWidget(item_name)
            wight.setLayout(layout_main)
            return wight

        for listWidget_item_data in listWidget_data:
            main_list_item = QListWidgetItem()
            main_list_item.setSizeHint(QSize(260, 80))
            main_list_widget = get_item_widget(listWidget_item_data)  # Call the above function to get the corresponding
            self.listWidget.addItem(main_list_item)  # Add item
            self.listWidget.setItemWidget(main_list_item, main_list_widget)  # Set widget for item

        # Add home window
        self.stackedWidget.removeWidget(self.page)
        self.stackedWidget.removeWidget(self.page_2)
        self.home_window = HomeWindow(self.username, self.userpassword, self.isuserlogin)
        self.home_window.home_login_signal.connect(self.login_info)
        # Add Email window
        self.home_window.home_email_signal.connect(self.home_email_info)
        self.stackedWidget.addWidget(self.home_window)
        self.email_w = EmailWindow()
        self.stackedWidget.addWidget(self.email_w)  # Set EmailWindow index=1
        self.home_window.home_setting_signal.connect(self.home_setting_info)

        self.setStyleSheet(SYS_STYLE)
        self.setWindowIcon(QIcon(APP_ICON))
        self.setWindowTitle('Webpages Monitor Robot')

    def init_slot(self):
        '''
        Connect the slot with list widget
        :return: None
        '''
        self.listWidget.currentItemChanged.connect(self.item_changed)

    def item_changed(self):
        '''
        If user choose different item, show different window
        :return: None
        '''
        if (self.listWidget.currentRow() == 0):
            self.stackedWidget.setCurrentIndex(self.listWidget.currentRow())

        if (self.listWidget.currentRow() == 1):
            # Determine whether a user is currently logged in
            if self.isuserlogin:
                print("User is login, open moodle page")
                self.moodle_window.re_draw()
                self.stackedWidget.setCurrentIndex(2)
            else:
                print("User is not login, open login page")
                self.moodle_login = LoginWindow()
                self.moodle_login.setWindowFlags(Qt.WindowStaysOnTopHint)
                self.moodle_login.show()
                self.moodle_login.login_signal.connect(self.login_info)

        if (self.listWidget.currentRow() == 2):
            # Email:  Determine whether a user is currently logged in
            if self.isloginEmail:
                print("email is login, open email page")
                self.email_w.re_draw(self.email_account, self.email_passwd)
                self.stackedWidget.setCurrentIndex(1)
            else:
                print("email is not login, open login page")
                self.email_login = EmailLoginWindow()
                self.email_login.setWindowFlags(QtCore.Qt.WindowStaysOnTopHint)
                self.email_login.show()
                self.email_login.login_signal.connect(self.email_login_info)

            # self.isloginEmail = True
            # self.email_w.re_draw()
            # self.stackedWidget.setCurrentIndex(1)

        if (self.listWidget.currentRow() == 3):
            # a. Initial interface
            # c. Initialize Email
            isloginMoodle = self.isuserlogin
            isloginEmail = self.isloginEmail
            self.moodle_setting = SettingWindow(isloginMoodle, isloginEmail, self.username, self.userpassword)
            self.moodle_setting.show()

            # if self.isuserlogin:
            #
            #     print("You need login first")
            # else:
            #     username = self.userinfo.split(' ')[0]
            #     password = self.userinfo.split(' ')[1]
            #     self.moodle_setting = SettingWindow(username, password)
            #     self.moodle_setting.show()
            self.moodle_setting.setting_signal.connect(self.setting_info)

    def home_setting_info(self):
        '''
        Connect the the setting button with slot
        :return: None
        '''
        isloginMoodle = self.isuserlogin
        isloginEmail = self.isloginEmail
        self.moodle_setting = SettingWindow(isloginMoodle, isloginEmail, self.username, self.userpassword)
        self.moodle_setting.show()

        # if self.isuserlogin:
        #
        #     print("You need login first")
        # else:
        #     username = self.userinfo.split(' ')[0]
        #     password = self.userinfo.split(' ')[1]
        #     self.moodle_setting = SettingWindow(username, password)
        #     self.moodle_setting.show()
        self.moodle_setting.setting_signal.connect(self.setting_info)

    def email_login_info(self, info, account, passwd):
        '''
        login email
        :param info: whether email login
        :param account: email username
        :param passwd: email information
        :return: None
        '''
        if info:
            self.isloginEmail = True
            self.email_account = account
            self.email_passwd = passwd
            self.email_w.re_draw(self.email_account, self.email_passwd)
            self.stackedWidget.setCurrentIndex(1)
        else:
            self.isloginEmail = False

    def setting_info(self, info):
        '''
        Slot for widgets in setting window
        :param info: information of widget
        :return: None
        '''
        if info == 'Update':
            print(
                "The Moodle module is updated by setting the course information that needs to be monitored in the setting")
            username = self.userinfo.split(' ')[0]
            password = self.userinfo.split(' ')[1]
            if self.moodle_window:
                self.stackedWidget.removeWidget(self.moodle_window)
            self.moodle_window_2 = MoodleWindow(username, password)
            self.stackedWidget.insertWidget(2, self.moodle_window_2)
            self.stackedWidget.setCurrentIndex(2)
        elif info == 'Logout':
            self.isuserlogin = False
            url_and_json_info.session = ''
        elif info == 'Logout_email':
            self.isloginEmail = False
            url_and_json_info.server = ''
        elif info == 're_draw':
            self.moodle_window.re_draw()

    def login_info(self, username, userpassword, isuserlogin):
        '''
        Login moodle
        :param username: moodle username
        :param userpassword: moodle password
        :param isuserlogin: whether login moodle
        :return: None
        '''
        if isuserlogin:
            # userinfo
            self.isuserlogin = isuserlogin
            self.username = username
            self.userpassword = userpassword
            # Add Moodle index=2
            self.moodle_window = MoodleWindow(username, userpassword)
            self.stackedWidget.insertWidget(2, self.moodle_window)
            self.stackedWidget.setCurrentIndex(2)
        else:
            print("pass is error")

    # def home_moodle_info(self, info):
    #     if info == 'get Moodle':
    #         self.userinfo = info
    #         # Add Moodle index=2
    #         username = self.userinfo.split(' ')[0]
    #         password = self.userinfo.split(' ')[1]
    #         self.moodle_window = MoodleWindow(username, password)
    #         self.stackedWidget.addWidget(self.moodle_window)
    #         self.stackedWidget.setCurrentIndex(2)
    #     else:
    #         print("pass is error")

    def home_email_info(self, info):
        '''
        Login email
        :param info: email login information
        :return: None
        '''
        if self.isloginEmail:
            print("email is login, open email page")
            self.email_w.re_draw(self.email_account, self.email_passwd)
            self.stackedWidget.setCurrentIndex(1)
        else:
            print("email is not login, open login page")
            self.email_login = EmailLoginWindow()
            self.email_login.setWindowFlags(QtCore.Qt.WindowStaysOnTopHint)
            self.email_login.show()
            self.email_login.login_signal.connect(self.email_login_info)


if __name__ == "__main__":
    app = QApplication(sys.argv)
    myWin = MainWindow()
    myWin.show()
    sys.exit(app.exec_())
