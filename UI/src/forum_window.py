'''
author: Team 16
'''

import sys
import time

from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from UI.UI_designer.home_window import Ui_home_Form
from UI.src.form_logic.Controller import *
from UI.util.common_util import *

'''
@desc: This is the class for forum window
'''

class Forum_Window(QWidget, Ui_home_Form):
    '''
    This is the class for forum window
    '''
    def __init__(self, username, password, url, parent=None):
        '''
        Init the forum window
        :param username: moodle username
        :param password: moodle passward
        :param url: moodle url
        :param parent: None
        Set up configuration of moodle
        If controller object not exists, create one
        '''
        super(Forum_Window, self).__init__(parent)
        self.setupUi(self)
        # Get the appropriate course information from the crawler
        self.username = username
        self.password = password
        if url_and_json_info.control != '':
            self.controller = url_and_json_info.control
            # print(1)
        else:
            self.controller = Controller()
            self.controller.login(username, password, True)
        self.url = url
        self.getForumData(url)
        self.init_ui()
        # self.init_slot()
        self.re_draw()

    # Redraw this window to update the window
    def re_draw(self):
        '''
        Re init the ui
        :return: None
        '''
        # self.getForumData(self.username,self.password,self.url)
        self.init_ui()
        # self.init_slot()

    def init_ui(self):
        '''
        Init the forum window ui
        :return: None
        Design the forum window widgets with codes directly
        Forum contents are divided into different group boxes
        '''
        # Use toolbox to achieve the button display effect
        self.forum_toolbox = QToolBox()
        self.forum_toolbox.layout().setSpacing(5)
        self.scrollArea.setWidget(self.forum_toolbox)
        # Distinguish forum into important and unimportant
        self.important_forum = []
        self.unimportant_forum = []
        self.forum_update = []
        self.ignore_forum_update = []

        for i in range(len(self.controller.user_forum_info['forum_update'])):
            ls = []
            ls.append(i)
            ls.append(self.controller.user_forum_info['forum_update'][i])
            self.forum_update.append(ls)
        for i in range(len(self.controller.user_forum_info['ignore_forum_update'])):
            ls = []
            ls.append(i)
            ls.append(self.controller.user_forum_info['ignore_forum_update'][i])
            self.ignore_forum_update.append(ls)
        for i in range(len(self.controller.user_forum_info['forum_history'])):
            ls = []
            ls.append(i)
            ls.append(self.controller.user_forum_info['forum_history'][i])
            self.important_forum.append(ls)
        for i in range(len(self.controller.user_forum_info['ignore_forum_history'])):
            ls = []
            ls.append(i)
            ls.append(self.controller.user_forum_info['ignore_forum_history'][i])
            self.unimportant_forum.append(ls)

        # Sets important and unimportant groupBox boxes
        # Important
        self.important_groupbox = QGroupBox()
        self.important_groupbox_layout = QVBoxLayout()
        self.important_groupbox_layout.setAlignment(Qt.AlignCenter)
        self.important_groupbox.setLayout(self.important_groupbox_layout)

        self.unimportant_groupbox = QGroupBox()
        self.unimportant_groupbox_layout = QVBoxLayout()
        self.unimportant_groupbox_layout.setAlignment(Qt.AlignCenter)
        self.unimportant_groupbox.setLayout(self.unimportant_groupbox_layout)

        self.history_important_groupbox = QGroupBox()
        self.hisyory_important_groupbox_layout = QVBoxLayout()
        self.hisyory_important_groupbox_layout.setAlignment(Qt.AlignCenter)
        self.history_important_groupbox.setLayout(self.hisyory_important_groupbox_layout)

        self.history_unimportant_groupbox = QGroupBox()
        self.hisyory_unimportant_groupbox_layout = QVBoxLayout()
        self.hisyory_unimportant_groupbox_layout.setAlignment(Qt.AlignCenter)
        self.history_unimportant_groupbox.setLayout(self.hisyory_unimportant_groupbox_layout)

        for topic_1 in self.forum_update:
            topic = topic_1[1]
            for item in topic['forum_data']:
                self.important_course_layout = QHBoxLayout()
                # self.important_course_layout.setStretch(0, 7)
                # self.important_course_layout.setStretch(1, 3)
                # Specify forum name and link
                self.important_course_info_layout = QVBoxLayout()
                self.forum_name = QLabel(topic['forum_name'])
                self.forum_name.setProperty('class', 'Forum')
                self.coursename = QLabel(item['discuss_title'])
                self.coursename.setProperty('class', 'Forum')
                self.courselink = QLabel('<a href="' + item['discuss_url'] + '">' + item['discuss_url'] + '</a>')
                self.courselink.setOpenExternalLinks(True)
                self.courselink.setProperty('class', 'Forum')
                self.courselink.setProperty('class', 'Forum')
                self.important_course_info_layout.addWidget(self.forum_name)
                self.important_course_info_layout.addWidget(self.coursename)
                self.important_course_info_layout.addWidget(self.courselink)
                # Define the ignore button
                isimportant_btn = QPushButton("Ignore")
                isimportant_btn.setProperty('class', 'Moodle')
                self.bind(isimportant_btn, topic['forum_url'], item['discuss_url'], False)
                # Assemble a layout
                self.important_course_layout.addLayout(self.important_course_info_layout)
                self.important_course_layout.addWidget(isimportant_btn)

                # Add a dividing line
                self.line_1 = QFrame()
                self.line_1.setStyleSheet("background-color: rgb(222, 222, 222);")
                self.line_1.setFrameShape(QFrame.HLine)
                self.line_1.setFrameShadow(QFrame.Sunken)
                # Add to groupbox
                self.important_groupbox_layout.addWidget(self.line_1)
                self.important_groupbox_layout.addLayout(self.important_course_layout)


        for topic_1 in self.ignore_forum_update:
            topic = topic_1[1]
            for item in topic['forum_data']:
                self.important_course_layout = QHBoxLayout()
                # self.important_course_layout.setStretch(0, 7)
                # self.important_course_layout.setStretch(1, 3)
                # Specify forum name and link
                self.important_course_info_layout = QVBoxLayout()
                self.forum_name = QLabel(topic['forum_name'])
                self.forum_name.setProperty('class', 'Forum')
                self.coursename = QLabel(item['discuss_title'])
                self.coursename.setProperty('class', 'Forum')
                self.courselink = QLabel('<a href="' + item['discuss_url'] + '">' + item['discuss_url'] + '</a>')
                self.courselink.setOpenExternalLinks(True)
                self.courselink.setProperty('class', 'Forum')
                self.important_course_info_layout.addWidget(self.forum_name)
                self.important_course_info_layout.addWidget(self.coursename)
                self.important_course_info_layout.addWidget(self.courselink)
                # Define the Subscribe button
                isimportant_btn = QPushButton("Subscribe")
                isimportant_btn.setProperty('class', 'Moodle')
                self.bind(isimportant_btn, topic['forum_url'], item['discuss_url'], True)
                # Assemble a layout
                self.important_course_layout.addLayout(self.important_course_info_layout)
                self.important_course_layout.addWidget(isimportant_btn)

                # Add a dividing line
                self.line_1 = QFrame()
                self.line_1.setStyleSheet("background-color: rgb(222, 222, 222);")
                self.line_1.setFrameShape(QFrame.HLine)
                self.line_1.setFrameShadow(QFrame.Sunken)
                # Add to Groupbox
                self.unimportant_groupbox_layout.addWidget(self.line_1)
                self.unimportant_groupbox_layout.addLayout(self.important_course_layout)
        # Add course information to the groupbox

        for topic_1 in self.unimportant_forum:
            topic = topic_1[1]
            for item in topic['forum_data']:
                self.important_course_layout = QHBoxLayout()
                # self.important_course_layout.setStretch(0, 7)
                # self.important_course_layout.setStretch(1, 3)
                # Specify forum name and link
                self.important_course_info_layout = QVBoxLayout()
                self.forum_name = QLabel(topic['forum_name'])
                self.forum_name.setProperty('class', 'Forum')
                self.coursename = QLabel(item['discuss_title'])
                self.coursename.setProperty('class', 'Forum')
                self.courselink = QLabel('<a href="' + item['discuss_url'] + '">' + item['discuss_url'] + '</a>')
                self.courselink.setOpenExternalLinks(True)
                self.courselink.setProperty('class', 'Forum')
                self.courselink.setProperty('class', 'Forum')
                self.important_course_info_layout.addWidget(self.forum_name)
                self.important_course_info_layout.addWidget(self.coursename)
                self.important_course_info_layout.addWidget(self.courselink)
                # Define the Subscribe button
                isimportant_btn = QPushButton("Subscribe")
                isimportant_btn.setProperty('class', 'Moodle')
                self.bind(isimportant_btn, topic['forum_url'], item['discuss_url'], True)
                # Assemble a layout
                self.important_course_layout.addLayout(self.important_course_info_layout)
                self.important_course_layout.addWidget(isimportant_btn)

                # Add a dividing line
                self.line_1 = QFrame()
                self.line_1.setStyleSheet("background-color: rgb(222, 222, 222);")
                self.line_1.setFrameShape(QFrame.HLine)
                self.line_1.setFrameShadow(QFrame.Sunken)
                # Add to Groupbox
                self.hisyory_unimportant_groupbox_layout.addWidget(self.line_1)
                self.hisyory_unimportant_groupbox_layout.addLayout(self.important_course_layout)

        for topic_1 in self.important_forum:
            topic = topic_1[1]
            for item in topic['forum_data']:
                self.important_course_layout = QHBoxLayout()
                # self.important_course_layout.setStretch(0, 7)
                # self.important_course_layout.setStretch(1, 3)
                # Specify forum name and link
                self.important_course_info_layout = QVBoxLayout()
                self.forum_name = QLabel(topic['forum_name'])
                self.forum_name.setProperty('class', 'Forum')
                self.coursename = QLabel(item['discuss_title'])
                self.coursename.setProperty('class', 'Forum')
                self.courselink = QLabel('<a href="' + item['discuss_url'] + '">' + item['discuss_url'] + '</a>')
                self.courselink.setOpenExternalLinks(True)
                self.courselink.setProperty('class', 'Forum')
                self.courselink.setProperty('class', 'Forum')
                self.important_course_info_layout.addWidget(self.forum_name)
                self.important_course_info_layout.addWidget(self.coursename)
                self.important_course_info_layout.addWidget(self.courselink)
                # Define the ignore button
                isimportant_btn = QPushButton("Ignore")
                isimportant_btn.setProperty('class', 'Moodle')
                self.bind(isimportant_btn, topic['forum_url'], item['discuss_url'], False)
                # Assemble a layout
                self.important_course_layout.addLayout(self.important_course_info_layout)
                self.important_course_layout.addWidget(isimportant_btn)

                # Add a dividing line
                self.line_1 = QFrame()
                self.line_1.setStyleSheet("background-color: rgb(222, 222, 222);")
                self.line_1.setFrameShape(QFrame.HLine)
                self.line_1.setFrameShadow(QFrame.Sunken)
                # Add to groupbox
                self.hisyory_important_groupbox_layout.addWidget(self.line_1)
                self.hisyory_important_groupbox_layout.addLayout(self.important_course_layout)

        self.forum_toolbox.addItem(self.important_groupbox, "Important")
        self.forum_toolbox.addItem(self.unimportant_groupbox, "Unimportant")
        self.forum_toolbox.addItem(self.history_important_groupbox, "Important History")
        self.forum_toolbox.addItem(self.history_unimportant_groupbox, "Unimportant History")

        # Add qss effect
        self.setStyleSheet(SYS_STYLE)
        self.setWindowIcon(QIcon(APP_ICON))
        self.setWindowTitle('Forum')

        url_and_json_info.last_renew_time = int(time.time())

    def bind(self, button, forum_url, ignore_url, s):
        '''
        Connect button with slot
        :param button: judge whether important
        :param forum_url: url for forum
        :param ignore_url: url for ignored forum
        :param s: True or False
        :return: None
        '''
        if s:
            button.clicked.connect(lambda: self.setimportant(forum_url, ignore_url))
        else:
            button.clicked.connect(lambda: self.setunimportant(forum_url, ignore_url))

    def setunimportant(self, forum_url, ignore_url):
        '''
        Ignore forum
        :param forum_url: url for forum
        :param ignore_url: url for ignored forum
        :return:
        '''
        self.controller.forum_ignore(forum_url, ignore_url)
        self.re_draw()

    def setimportant(self, forum_url, ignore_url):
        '''
        Remove ignored forum
        :param forum_url: url for forum
        :param ignore_url: url for ignored forum
        :return:
        '''
        self.controller.remove_forum_ignore(forum_url, ignore_url)
        self.re_draw()

    # Extract forum information from the back end
    def getForumData(self, url):
        '''
        Adjust forum
        :param url: forum url
        :return: None
        '''
        a, b, c, d, e, f = self.controller.forum_scan(url)
        self.controller.forum_adjusting()


