'''
author: Team 16
'''

import sys
import time

from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from UI.UI_designer.email_login_window import *
from UI.src.email_spider import newEmail
from UI.src.form_logic.Controller import *
from UI.src.form_logic.FileForMac import File
from UI.util.common_util import *

'''
@desc: This is the class for email login
'''

class EmailLoginWindow(QWidget, Ui_email_LoginForm):
    '''
    This is the class for eamil login
    '''
    # create a signal judge that login is successful or not
    # and pass the username and password with this information
    login_signal = pyqtSignal(bool,str,str)
    def __init__(self, parent=None):
        '''
        This is the constructor of EmailLoginWindow
        :param parent: None
        Init the EmailLoginWindow
        '''
        super(EmailLoginWindow, self).__init__(parent)
        self.file = File()
        self.setupUi(self)
        self.init_ui()
        self.init_slot()

    def init_ui(self):
        """
        Initialize the interface UI elements
        :return: None
        Set window style
        Set button property
        """
        self.setWindowTitle('login for email')
        self.setWindowIcon(QIcon(APP_ICON))
        self.login_pushButton.setProperty('class', 'Aqua')
        self.register_pushButton.setProperty('class', 'Aqua')
        self.setStyleSheet(SYS_STYLE)
        self.setWindowFlags(Qt.WindowCloseButtonHint|Qt.WindowMinimizeButtonHint)

    def init_slot(self):
        """
        Initialize the signal slot connection
        :return None
        Connect button with btn_slot
        """
        self.register_pushButton.clicked.connect(lambda: self.btn_slot('cancel'))
        self.login_pushButton.clicked.connect(lambda: self.btn_slot('login'))
        # self.login_done_signal.connect(self.handle_login)

    def btn_slot(self, tag):
        """
        Button click event slot function
        :param tag: Tag of the button that was clicked
        :return: return if error, no subsequent operation logic will be performed
        If login successfully, get user information
        And if user information not exists, create user information file
        """
        # cancel
        if tag == 'cancel':
            # close login window
            self.close()

        # login
        if tag == 'login':
            username = self.username_lineEdit.text()
            password = self.password_lineEdit.text()
            if '' in [username, password]:
                msg_box(self, 'Prompt', 'Please input username or password!')
                return

            # get the entered username and password
            print("username: " + username + ",password： " + password)
            # judge whether user is successfully login or not
            try:
                self.server = newEmail(username,password)
                self.server.get_mails()
                url_and_json_info.server = self.server
                url_and_json_info.last_renew_time_email = int(time.time())

                email_info = self.file.loadJson(self.file.user_info_absolute_path + str(username))
                # print(self.file.user_info_absolute_path + str(username))
                if not email_info:
                    email_info = {}
                    email_info['username'] = username
                    email_info['password'] = password
                    email_info['keywords'] = ''
                    email_info['contacts'] = ''
                    email_info['monitor_interval'] = url_and_json_info.monitor_interval
                    email_info['update_time'] = url_and_json_info.update_time
                    self.file.saveJson(self.file.user_info_path, email_info, str(username))
                # else:
                #     print(1)

                url_and_json_info.email_info = email_info
                # print(url_and_json_info.email_info)

                url_and_json_info.email_contacts = url_and_json_info.email_info['contacts'].split(' ')
                url_and_json_info.email_keywords = url_and_json_info.email_info['keywords'].split(' ')
                if url_and_json_info.email_contacts[0] == '':
                    url_and_json_info.email_contacts = []
                if url_and_json_info.email_keywords[0] == '':
                    url_and_json_info.email_keywords = []

                url_and_json_info.monitor_interval = url_and_json_info.email_info['monitor_interval']
                url_and_json_info.update_time = url_and_json_info.email_info['update_time']

                self.login_signal.emit(True, username, password)
                self.close()
            except Exception as e:
                print("login failed:" + str(e))
                self.login_signal.emit(False, '','')
                QMessageBox.about(self, "ERROR", str(e))

