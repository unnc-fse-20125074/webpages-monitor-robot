'''
author: Team 16
'''

import json
import os
import tkinter as tk
from tkinter import filedialog
from UI.UI_designer.setting_window import *
from UI.src.form_logic.FileForMac import File
from UI.src.form_logic.Url_json_list import url_and_json_info
from UI.src.monitor_window import *
from UI.src.pop_window import Pop_Window

"""
@desc: This is the setting window
"""

class SettingWindow(QMainWindow, Ui_SettingWindow):
    '''
    This is the setting window
    '''
    # Create a signal indicating whether the login was successful or not
    setting_signal = pyqtSignal(str)

    def __init__(self, isLoginMoodle, isLoginEmail, username, passwd, parent=None):
        '''
        Init setting window ui
        :param isLoginMoodle: whether login moodle
        :param isLoginEmail: whether login email
        :param username: moodle username
        :param passwd: username password
        :param parent: None
        '''
        super(SettingWindow, self).__init__(parent)
        self.setupUi(self)
        self.isloginMoodle = isLoginMoodle
        self.isloginEmail = isLoginEmail
        self.username = username
        self.passwd = passwd
        self.init_ui()
        self.init_slot()



    def init_ui(self):
        """
        Initialize the interface UI elements
        :return: None
        There are three pages, general, moodle and email
        """
        self.setWindowTitle('Setting')
        self.setWindowIcon(QIcon(APP_ICON))
        # self.save_pushButton.setProperty('class', 'Aqua')
        # self.cancel_pushButton.setProperty('class', 'Aqua')
        # (1) Initial state
        self.listWidget.setProperty('class', 'Normal')
        self.listWidget.setCurrentRow(0)
        self.stackedWidget.removeWidget(self.page)
        self.stackedWidget.removeWidget(self.page_2)
        # General
        general_list_item = QListWidgetItem()
        general_list_item.setSizeHint(QSize(200, 80))
        gengeral_item_name = QLabel("General")
        gengeral_item_name.setStyleSheet(
            "QLabel{color:rgb(0,0,205);font-size:18px;font-weight:normal;font-family:Arial;}")
        gengeral_item_name.setAlignment(Qt.AlignCenter)
        self.listWidget.addItem(general_list_item)
        self.listWidget.setItemWidget(general_list_item, gengeral_item_name)
        # Add the corresponding interface for general
        general_widget = QWidget()

        general_layout = QVBoxLayout()
        # a
        general_item_layout1 = QHBoxLayout()
        update_label = QLabel("Update time : ")
        self.update_edit = QLineEdit(url_and_json_info.update_time)
        general_item_layout1.addWidget(update_label)
        general_item_layout1.addWidget(self.update_edit)
        general_layout.addLayout(general_item_layout1)

        # b
        general_item_layout2 = QHBoxLayout()
        Monitor_label = QLabel("Monitor interval : ")
        self.Monitor_edit = QLineEdit(url_and_json_info.monitor_interval)
        general_item_layout2.addWidget(Monitor_label)
        general_item_layout2.addWidget(self.Monitor_edit)
        general_layout.addLayout(general_item_layout2)

        # c
        general_item_layout3 = QHBoxLayout()
        self.cancel_ptn1 = QPushButton("Cancel")
        self.save_ptn1 = QPushButton("Save")

        self.cancel_ptn1.setProperty('class', 'Aqua')
        self.save_ptn1.setProperty('class', 'Aqua')

        general_item_layout3.addWidget(self.cancel_ptn1)
        general_item_layout3.addWidget(self.save_ptn1)
        general_layout.addLayout(general_item_layout3)

        general_widget.setLayout(general_layout)
        self.stackedWidget.addWidget(general_widget)

        # (2) Moodle
        if self.isloginMoodle:
            moodle_list_item = QListWidgetItem()
            moodle_list_item.setSizeHint(QSize(200, 80))
            moodle_item_name = QLabel("Moodle")
            moodle_item_name.setStyleSheet(
                "QLabel{color:rgb(0,0,205);font-size:18px;font-weight:normal;font-family:Arial;}")
            moodle_item_name.setAlignment(Qt.AlignCenter)
            self.listWidget.addItem(moodle_list_item)
            self.listWidget.setItemWidget(moodle_list_item, moodle_item_name)

            # Add the corresponding interface for moodle
            moodle_widget = QWidget()

            moodle_layout = QVBoxLayout()
            # a
            moodle_item_layout1 = QHBoxLayout()
            download_label = QLabel("Download path: ")
            self.download_edit = QLineEdit(url_and_json_info.user_info['download_path'])
            self.download_button = QPushButton('Src')
            moodle_item_layout1.addWidget(download_label)
            moodle_item_layout1.addWidget(self.download_edit)
            moodle_item_layout1.addWidget(self.download_button)
            moodle_layout.addLayout(moodle_item_layout1)

            # b
            moodle_item_layout2 = QHBoxLayout()
            self.change_monitor_ptn = QPushButton("Change monitor module")
            self.change_monitor_ptn.setProperty('class', 'Aqua')
            moodle_item_layout2.addWidget(self.change_monitor_ptn)
            moodle_layout.addLayout(moodle_item_layout2)

            # c
            moodle_item_layout3 = QHBoxLayout()
            self.logout_ptn2 = QPushButton("Log out")
            self.cancel_ptn2 = QPushButton("Cancel")
            self.save_ptn2 = QPushButton("Save")

            self.logout_ptn2.setProperty('class', 'Aqua')
            self.cancel_ptn2.setProperty('class', 'Aqua')
            self.save_ptn2.setProperty('class', 'Aqua')

            moodle_item_layout3.addWidget(self.logout_ptn2)
            moodle_item_layout3.addWidget(self.cancel_ptn2)
            moodle_item_layout3.addWidget(self.save_ptn2)
            moodle_layout.addLayout(moodle_item_layout3)

            moodle_widget.setLayout(moodle_layout)
            self.stackedWidget.addWidget(moodle_widget)

        # (3) Email
        if self.isloginEmail:
            email_list_item = QListWidgetItem()
            email_list_item.setSizeHint(QSize(200, 80))
            email_item_name = QLabel("Email")
            email_item_name.setStyleSheet(
                "QLabel{color:rgb(0,0,205);font-size:18px;font-weight:normal;font-family:Arial;}")
            email_item_name.setAlignment(Qt.AlignCenter)
            self.listWidget.addItem(email_list_item)
            self.listWidget.setItemWidget(email_list_item, email_item_name)

            # Add the corresponding interface for email
            email_widget = QWidget()

            email_layout = QVBoxLayout()
            # a
            email_item_layout1 = QHBoxLayout()
            keywords_label = QLabel("Add key words : ")
            self.keywords_edit = QLineEdit('')
            self.show_keywords = QPushButton('show keywords')
            self.show_keywords.setFixedWidth(200)
            self.show_keywords.clicked.connect(self.show_key)
            email_item_layout1.addWidget(keywords_label)
            email_item_layout1.addWidget(self.keywords_edit)
            email_item_layout1.addWidget(self.show_keywords)
            email_layout.addLayout(email_item_layout1)

            # b
            email_item_layout2 = QHBoxLayout()
            import_label = QLabel("Add important contact : ")
            self.import_edit = QLineEdit('')
            self.show_contact = QPushButton('show contact')
            self.show_contact.setFixedWidth(200)
            self.show_contact.clicked.connect(self.show_con)
            email_item_layout2.addWidget(import_label)
            email_item_layout2.addWidget(self.import_edit)
            email_item_layout2.addWidget(self.show_contact)
            email_layout.addLayout(email_item_layout2)

            # c
            email_item_layout3 = QHBoxLayout()
            re_keywords_label = QLabel("Remove key words : ")
            self.re_keywords_edit = QLineEdit('')
            email_item_layout3.addWidget(re_keywords_label)
            email_item_layout3.addWidget(self.re_keywords_edit)
            email_layout.addLayout(email_item_layout3)

            # d
            email_item_layout4 = QHBoxLayout()
            re_import_label = QLabel("Remove important contact : ")
            self.re_import_edit = QLineEdit('')
            email_item_layout4.addWidget(re_import_label)
            email_item_layout4.addWidget(self.re_import_edit)
            email_layout.addLayout(email_item_layout4)

            # c
            email_item_layout5 = QHBoxLayout()
            self.logout_ptn3 = QPushButton("Log out")
            self.cancel_ptn3 = QPushButton("Cancel")
            self.save_ptn3 = QPushButton("Save")

            self.logout_ptn3.setProperty('class', 'Aqua')
            self.cancel_ptn3.setProperty('class', 'Aqua')
            self.save_ptn3.setProperty('class', 'Aqua')

            email_item_layout5.addWidget(self.logout_ptn3)
            email_item_layout5.addWidget(self.cancel_ptn3)
            email_item_layout5.addWidget(self.save_ptn3)
            email_layout.addLayout(email_item_layout5)

            email_widget.setLayout(email_layout)
            self.stackedWidget.addWidget(email_widget)

        self.setStyleSheet(SYS_STYLE)
        self.setWindowFlags(Qt.WindowCloseButtonHint | Qt.WindowMinimizeButtonHint)



    def cancel(self):
        '''
        Slot to close the setting window
        :return: None
        '''
        self.close()

    def rewrite_downpath(self):
        '''
        Get the new download path
        :return: None
        '''
        root = tk.Tk()
        root.withdraw()
        dirpath = filedialog.askdirectory()
        self.download_edit.setText(os.path.normpath(dirpath))

    def show_con(self):
        '''
        Using pop window to show the contacts
        :return: None
        '''
        lst = []
        for item in url_and_json_info.email_contacts:
            l = []
            l.append(item)
            l.append('')
            lst.append(l)
        self.pop = Pop_Window(lst)
        self.pop.setWindowTitle('contacts')
        self.pop.show()

    def show_key(self):
        '''
        Using pop window to show the key words
        :return: None
        '''
        lst = []
        for item in url_and_json_info.email_keywords:
            l = []
            l.append(item)
            l.append('')
            lst.append(l)
        self.pop = Pop_Window(lst)
        self.pop.setWindowTitle('keywords')
        self.pop.show()

    def general_save(self):
        '''
        Save the update time and monitor interval
        :return: None
        '''
        url_and_json_info.update_time = self.update_edit.text()
        url_and_json_info.monitor_interval = self.Monitor_edit.text()
        if url_and_json_info.email_info:
            url_and_json_info.email_info['update_time'] = self.update_edit.text()
        if url_and_json_info.user_info:
            url_and_json_info.user_info['update_time'] = self.update_edit.text()
        if url_and_json_info.email_info:
            url_and_json_info.email_info['monitor_interval'] = self.Monitor_edit.text()
        if url_and_json_info.user_info:
            url_and_json_info.user_info['monitor_interval'] = self.Monitor_edit.text()
        file = File()
        if url_and_json_info.user_info:
            file.saveJson(file.user_info_path, url_and_json_info.user_info, url_and_json_info.user_info['username'])
        if url_and_json_info.email_info:
            file.saveJson(file.user_info_path, url_and_json_info.email_info, url_and_json_info.email_info['username'])

        self.close()

    def moodle_save(self):
        '''
        Save the download path
        :return:
        '''
        url_and_json_info.download_path = self.download_edit.text()
        file = File()
        url_and_json_info.user_info['download_path'] = self.download_edit.text()
        file.saveJson(file.user_info_path, url_and_json_info.user_info, url_and_json_info.user_info['username'])

        self.close()


    def email_save(self):
        '''
        Save the settings fot emali
        :return: None
        '''
        l = ''
        if self.keywords_edit.text() != '':
            url_and_json_info.email_keywords.append(self.keywords_edit.text())
            url_and_json_info.email_info['keywords'] = ' '.join(url_and_json_info.email_keywords)
            l += 'Succeed to add new keyword:' + self.keywords_edit.text() + '\n'
        # print(1)
        if self.import_edit.text() != '':
            url_and_json_info.email_contacts.append(self.import_edit.text())
            url_and_json_info.email_info['contacts'] = ' '.join(url_and_json_info.email_contacts)
            l += 'Succeed to add new contact:' + self.import_edit.text() + '\n'
        # print(1)
        if self.re_keywords_edit.text() != '':
            try:
                url_and_json_info.email_keywords.remove(self.re_keywords_edit.text())
                url_and_json_info.email_info['keywords'] = ' '.join(url_and_json_info.email_keywords)
                l += 'Succeed to remove keyword:' + self.re_keywords_edit.text() + '\n'
            except ValueError:
                l += 'Failed to remove keyword:' + self.re_keywords_edit.text() + '\n'
        # print(1)
        if self.re_import_edit.text() != '':
            try:
                url_and_json_info.email_contacts.remove(self.re_import_edit.text())
                url_and_json_info.email_info['contacts'] = ' '.join(url_and_json_info.email_contacts)
                l += 'Succeed to remove contact:' + self.re_import_edit.text() + '\n'
            except ValueError:
                l += 'Failed to remove contact:' + self.re_import_edit.text() + '\n'
        file = File()
        file.saveJson(file.user_info_path, url_and_json_info.email_info, url_and_json_info.email_info['username'])

        QMessageBox.about(self, "INFO", l)

        self.close()

    def init_slot(self):
        """
        Initialize the signal slot connection
        :return: None
        """
        self.listWidget.currentItemChanged.connect(self.item_changed)
        self.cancel_ptn1.clicked.connect(self.cancel)
        self.save_ptn1.clicked.connect(self.general_save)

        if self.isloginMoodle:
            self.logout_ptn2.clicked.connect(lambda: self.btn_slot('logout'))
            self.change_monitor_ptn.clicked.connect(lambda: self.btn_slot('change_monitor'))
            self.download_button.clicked.connect(self.rewrite_downpath)
            self.cancel_ptn2.clicked.connect(self.cancel)
            self.save_ptn2.clicked.connect(self.moodle_save)
        if self.isloginEmail:
            self.logout_ptn3.clicked.connect(lambda: self.btn_slot('logout_email'))
            self.cancel_ptn3.clicked.connect(self.cancel)
            self.save_ptn3.clicked.connect(self.email_save)


    def item_changed(self):
        '''
        Change the setting pages between general, moodle and email
        :return: None
        '''
        if (self.listWidget.currentRow() == 0):
            self.stackedWidget.setCurrentIndex(self.listWidget.currentRow())

        if (self.listWidget.currentRow() == 1):
            self.stackedWidget.setCurrentIndex(self.listWidget.currentRow())

        if (self.listWidget.currentRow() == 2):
            self.stackedWidget.setCurrentIndex(self.listWidget.currentRow())

    def btn_slot(self, tag):
        """
        Button click event slot function
        :param tag: Tag of the button that was clicked
        :return: return if error, no subsequent operation logic will be performed
        """
        # logout
        if tag == 'logout':
            # Click Logout to prompt a new window: the current account file will not be preserved
            reply = QMessageBox.information(self,  # Use the information message box
                                            "Alert",
                                            "Are you sure to log out?",
                                            QMessageBox.Yes | QMessageBox.No)
            if reply == QMessageBox.Yes:
                self.setting_signal.emit('Logout')
            self.close()
        if tag == 'logout_email':
            # Click Logout to prompt a new window: the current account file will not be preserved
            reply = QMessageBox.information(self,  # Use the information message box
                                            "Alert",
                                            "Are you sure to log out?",
                                            QMessageBox.Yes | QMessageBox.No)
            if reply == QMessageBox.Yes:
                self.setting_signal.emit('Logout_email')
            self.close()

        # change monitor
        if tag == 'change_monitor':
            # Pop-up a new window with check boxes for all courses
            print("# Pop-up a new window with check boxes for all courses")
            self.monitor_window = Monitor_Window(self.username, self.passwd)
            self.monitor_window.monitor_signal.connect(lambda: self.btn_slot('re_draw'))
            self.monitor_window.show()

        if tag == 're_draw':
            self.setting_signal.emit('re_draw')

        # Cancel
        if tag == 'cancel':
            #  Moodle will be updated by passing information to the main page at the same time
            self.close()

        # Login
        if tag == 'save':
            update_time = self.comboBox_1.currentText()
            moniter_interval = self.comboBox_2.currentText()
            adding_monitor_modules = self.comboBox_3.currentText()
            moniter_area = self.comboBox_4.currentText()
            download_path = self.down_lineEdit.text()
            # Create a new list to pass as a signal
            setting_signal_list = []
            setting_signal_list.append({'update_time': update_time})
            setting_signal_list.append({'moniter_interval': moniter_interval})
            setting_signal_list.append({'adding_monitor_modules': adding_monitor_modules})
            setting_signal_list.append({'moniter_area': moniter_area})
            # Add a link for the course
            for course_item_data in self.course_list_data:
                if course_item_data['courseName'] == adding_monitor_modules:
                    course_item_url = course_item_data['courseUrl']
                    setting_signal_list.append({'course_item_url': course_item_url})
            # If the download_path is empty, it is added to the current address as the download address
            if download_path == '':
                new_download_path = os.path.join(USER_ALL_COURSE, 'data', self.username, adding_monitor_modules)
                setting_signal_list.append({'new_download_path': new_download_path})
            else:
                new_download_path = os.path.join(download_path, adding_monitor_modules)
                setting_signal_list.append({'new_download_path': new_download_path})

            courseinfofile(self, update_time, moniter_interval, adding_monitor_modules, moniter_area, course_item_url,
                           new_download_path)
            # Moodle will be updated by passing information to the main page at the same time
            self.setting_signal.emit("Update")
            self.close()

def courseinfofile(self, new_update_time, new_moniter_interval, new_adding_monitor_modules, new_moniter_area, new_course_item_url, new_new_download_path):
    '''
    Setting the monitor modules
    :param self: setting window
    :param new_update_time: update time
    :param new_moniter_interval: moniter interval
    :param new_adding_monitor_modules: new added moniter modules
    :param new_moniter_area: new area to monitor
    :param new_course_item_url: url for course
    :param new_new_download_path: the download path
    :return: Noe
    '''
    user_monitoring_path = os.path.join(USER_ALL_COURSE, 'data', self.username) # Monitor the course store information
    if os.path.exists(user_monitoring_path):
        print("The user folder already exists")
        user_monitoring_course_path = os.path.join(user_monitoring_path, 'monitoring_course.json')
        if (os.path.exists(user_monitoring_course_path)):
            with open(user_monitoring_course_path, 'r') as f:
                load_dict = json.load(f)
                num_course = len(load_dict)
                course_list = []
                for course in range(num_course):
                    update_time = load_dict[course]["update_time"]
                    moniter_interval = load_dict[course]["moniter_interval"]
                    adding_monitor_modules = load_dict[course]["adding_monitor_modules"]
                    moniter_area = load_dict[course]["moniter_area"]
                    course_item_url = load_dict[course]["course_item_url"]
                    new_download_path = load_dict[course]["new_download_path"]

                    course_dict = {'update_time': update_time, 'moniter_interval': moniter_interval,
                                   'adding_monitor_modules': adding_monitor_modules,
                                   'moniter_area': moniter_area, 'course_item_url': course_item_url,
                                   'new_download_path': new_download_path}
                    course_list.append(course_dict)  # Append the file according to the append of the list
                f.close()
            # flag = 1 exists
            flag = 0
            for item in course_list:
                if item['adding_monitor_modules'] == new_adding_monitor_modules:
                    print("It already exists, doesn't add")
                    flag = 1
            if flag == 0:  # doesn't exist
                new_course_dict = {'update_time': new_update_time, 'moniter_interval': new_moniter_interval,
                                   'adding_monitor_modules': new_adding_monitor_modules,
                                   'moniter_area': new_moniter_area, 'course_item_url': new_course_item_url,
                                   'new_download_path': new_new_download_path}
                course_list.append(new_course_dict)

                with open(user_monitoring_course_path, 'w', encoding='utf-8') as file:
                    json.dump(course_list, file, ensure_ascii=False)
                    file.close()
        else:
            print("Create new folder")
            course_list = []
            new_course_dict = {'update_time': new_update_time, 'moniter_interval': new_moniter_interval,
                               'adding_monitor_modules': new_adding_monitor_modules,
                               'moniter_area': new_moniter_area, 'course_item_url': new_course_item_url,
                               'new_download_path': new_new_download_path}
            course_list.append(new_course_dict)
            with open(user_monitoring_course_path, 'w', encoding='utf-8') as file:
                json.dump(course_list, file, ensure_ascii=False)
                file.close()
    else:
        print("The user folder does not exist and needs to be logged in!")


if __name__ == "__main__":
    app = QApplication(sys.argv)
    myWin = SettingWindow(True, True)
    myWin.show()
    sys.exit(app.exec_())
