'''
author: Team 16
'''

import json
import os
import sys
import threading

from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from UI.UI_designer.home_window import Ui_home_Form
from UI.src.form_logic.Url_json_list import url_and_json_info
from UI.src.forum_window import Forum_Window
from UI.src.moodle_spider import newCnt, urlAnalysis
from UI.util.common_util import *


class MoodleWindow(QWidget, Ui_home_Form):
    '''
    Responsible for UI of Moodle Window.
    '''

    def __init__(self, username, password, parent=None):
        super(MoodleWindow, self).__init__(parent)
        self.setupUi(self)
        self.username = username
        self.password = password
        self.getSession()
        # Get the appropriate course information from the crawler
        # self.getCourseData()
        self.init_ui()
        self.init_slot()
        self.dir = None

    def re_draw(self):
        '''
        Reinitialize Moodle Window.
        '''
        self.getSession()
        self.init_ui()

    def getSession(self):
        '''
        Obtain session and information of a certain monitored course.
        '''
        # Climb data from a crawler
        self.session = newCnt(self.username, self.password)
        # Get data
        from UI.src.form_logic.FileForMac import File
        file = File()

        user_monitoring_path = file.user_info_absolute_path
        # print(user_monitoring_path)
        if os.path.exists(user_monitoring_path):
            print("The user folder already exists")
            user_monitoring_course_path = os.path.join(user_monitoring_path, self.username + '_courseList')
            print(user_monitoring_course_path)
            if (os.path.exists(user_monitoring_course_path)):
                with open(user_monitoring_course_path, 'r') as f:
                    load_dict = json.load(f)
                    print(load_dict)
                    num_course = len(load_dict)
                    course_list = []
                    for course in range(num_course):
                        course_name = load_dict[course]["course_name"]
                        course_url = load_dict[course]["course_url"]
                        course_monitoring = load_dict[course]["monitoring"]
                        course_id = load_dict[course]["id"]
                        course_dict = {'course_name': course_name, 'course_url': course_url,
                                       'course_monitoring': course_monitoring,
                                       'course_id': course_id}
                        course_list.append(course_dict)  # Append the file according to the append of the list

                    f.close()
                self.data = course_list
                # print(self.data)
        else:
            print("The user folder does not exist and needs to be logged in!")
            self.data = []

    def init_ui(self):
        '''
        Initialize the interface UI elements.
        '''
        # Use toolbox to achieve the button display effect
        self.moodle_toolbox = QToolBox()
        self.moodle_toolbox.layout().setSpacing(5)
        self.scrollArea.setWidget(self.moodle_toolbox)
        # Get data
        for i in range(self.moodle_toolbox.count()):
            self.moodle_toolbox.removeItem(0)

        for course_item_data in self.data:
            if course_item_data['course_monitoring']:
                self.moodle_groupbox = QGroupBox()
                self.moodle_groupbox.setFixedSize(600, 180)
                self.moodle_groupbox_layout = QVBoxLayout()
                self.moodle_groupbox_layout.setAlignment(Qt.AlignCenter)
                self.moodle_groupbox.setLayout(self.moodle_groupbox_layout)

                self.moodle_btn_1 = QPushButton()
                self.moodle_btn_1.setText("New Class Material")
                self.moodle_btn_1.setFixedSize(450, 50)
                self.moodle_btn_1.setProperty('class', 'Moodle')
                self.moodle_btn_1.clicked.connect(lambda: self.btn1(self.moodle_toolbox.currentIndex()))

                self.moodle_btn_2 = QPushButton()
                self.moodle_btn_2.setText("Forum updates")
                self.moodle_btn_2.setFixedSize(450, 50)
                self.moodle_btn_2.setProperty('class', 'Moodle')
                self.moodle_btn_2.clicked.connect(lambda: self.btn2(self.moodle_toolbox.currentIndex()))

                # self.moodle_btn_3 = QPushButton()
                # self.moodle_btn_3.setText("Announcement Updates")
                # self.moodle_btn_3.setFixedSize(450, 50)
                # self.moodle_btn_3.setProperty('class', 'Moodle')
                # self.moodle_btn_3.clicked.connect(lambda: self.btn3(self.moodle_toolbox.currentIndex()))

                self.moodle_groupbox_layout.addWidget(self.moodle_btn_1)
                self.moodle_groupbox_layout.addWidget(self.moodle_btn_2)
                # self.moodle_groupbox_layout.addWidget(self.moodle_btn_3)

                self.moodle_toolbox.addItem(self.moodle_groupbox, course_item_data['course_name'])

        # Add qss effect
        self.setStyleSheet(SYS_STYLE)
        self.setWindowIcon(QIcon(APP_ICON))
        self.setWindowTitle('Moodle')

    def init_slot(self):
        '''
        Initialize the signal slot connection.
        '''
        # print("")
        # self.moodle_listWidget.currentItemChanged.connect(self.item_changed)

    def auto_downfile(self):
        '''
        Download all files automatically by calling btn1 in a loop.
        '''
        for i in range(self.moodle_toolbox.count()):
            self.btn1(i)

    # Download files
    def btn1(self, current_index):
        '''
        button to download files of a certain course.
        :param current_index : index of the course in the course list.
        '''
        i = 0
        tt = ''
        for item in self.data:
            if item['course_monitoring']:
                if i == current_index:
                    tt = item
                    break
                else:
                    i += 1
        self.courseName = tt['course_name']
        self.courseUrl = tt['course_url']
        # down pdf
        self.dir = url_and_json_info.download_path
        downpdf = threading.Thread(target=downfile, args=(
        self.session, self.courseName, self.courseUrl, url_and_json_info.download_path))
        # Specify the task to be performed by the child thread through target.You can specify arguments to test1 with args= tuple.
        downpdf.start()
        # The child thread is created and executed only after the start method is called
        # QMessageBox.information(self, "Prompt", "Start download", QMessageBox.Yes)

    def btn2(self, current_index):
        '''
        Pass the current monitored course' url to Forum Window.
        :param current_index : index of the course in the course list.
        '''

        # Pass the current course name
        i = 0
        tt = ''
        for item in self.data:
            if item['course_monitoring']:
                if i == current_index:
                    tt = item
                    break
                else:
                    i += 1
        print(tt)
        print('-----')
        self.update_window = Forum_Window(self.username, self.password, tt['course_url'])
        self.update_window.show()


def downfile(session, courseName, courseUrl, dir):
    '''
    Responsible for download courseware in the window.
    :param session : session with Moodle server
    :param courseName : name of a certain course
    :param courseUrl : url of the homepage of a certain course
    :param dir : path to store all information crawled of a certain course
    '''
    print("Download files...")
    print(courseName)
    print(courseUrl)
    urlAnalysis(session, courseName, courseUrl, dir)

