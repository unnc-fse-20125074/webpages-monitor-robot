'''
author: Team 16
'''

import poplib
import zmail
from UI.src.form_logic.Url_json_list import url_and_json_info

poplib._MAXLINE = 65535

'''
@desc: This is the class for email server
'''


def newEmail(username, password):
    '''
    This is the class for email server
    :param username: email username
    :param password: email password
    :return: email server
    Get email server
    If email server not exists, create one
    '''

    if url_and_json_info.server != '':
        return url_and_json_info.server
    server = zmail.server(username, password)
    return server


if __name__ == "__main__":
    # input username and password
    username = 'testforwmr@163.com'
    password = 'TCWHDBTQUBPGXXXX'
    server = newEmail(username, password)

    mail = {
        'subject': 'hello!',
        'content_text': 'This message from zmail!',
    }

    server.send_mail('testforwmr@163.com', mail)

    # # get the latest email
    # mails = server.get_mails()
    # print(mails)
    # # mail=server.get_mail(1)
    # # read email
    # for mail in mails:
    #     print(mail['id'])
    #     print(mail['subject'])
    #     print(mail['from'])
    #     print(mail['content_text'])
    #     print(int(time.mktime(mail['date'].timetuple())))
    #     print(datetime.datetime.strftime(mail['date'],"%Y-%m-%d %H:%M:%S"))
