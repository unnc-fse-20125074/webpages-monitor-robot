'''
author: Team 16
'''

from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from UI.UI_designer.announce_window import *
from UI.util.common_util import APP_ICON, SYS_STYLE

"""
@desc: This is the design of UI for show email window
"""

class ShowEmailWindow(QWidget, Ui_AnnounceForm):
    '''
    This is the design of UI for show email window
    '''
    def __init__(self, parent=None):
        '''
        Init the Show email window
        :param parent: None
        '''
        super(ShowEmailWindow, self).__init__(parent)
        self.setupUi(self)
        self.init_ui()

    def init_ui(self):
        """
        Initialize the interface UI elements
        :return: None
        Set window style
        """
        self.setWindowTitle('ShowEmailWindow')
        self.setWindowIcon(QIcon(APP_ICON))
        self.setStyleSheet(SYS_STYLE)
        self.setWindowFlags(Qt.WindowCloseButtonHint|Qt.WindowMinimizeButtonHint)

