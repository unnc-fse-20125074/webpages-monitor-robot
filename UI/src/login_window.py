'''
author: Team 16
'''

import os
import sys
import time
import threading
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from UI.UI_designer.login_window import *
from UI.src.form_logic.Controller import *
from UI.src.moodle_spider import newCnt, getList
from UI.util.common_util import *


class LoginWindow(QWidget, Ui_LoginForm):
    '''
    Responsible for the UI of login Moodle.
    '''

    # create a signal judge that login is successful or not
    # and pass the username and password with this information
    login_signal = pyqtSignal(str, str, bool)
    def __init__(self, parent=None):
        super(LoginWindow, self).__init__(parent)
        self.setupUi(self)
        self.init_ui()
        self.init_slot()
        # ADD Moodle logic
        self.moodle_controler = Controller()

    def init_ui(self):
        """
        Initialize the interface UI elements
        """
        self.setWindowTitle('login for moodle')
        self.setWindowIcon(QIcon(APP_ICON))
        self.login_pushButton.setProperty('class', 'Aqua')
        self.register_pushButton.setProperty('class', 'Aqua')
        self.setStyleSheet(SYS_STYLE)
        self.setWindowFlags(Qt.WindowCloseButtonHint|Qt.WindowMinimizeButtonHint)

    def init_slot(self):
        """
        Initialize the signal slot connection.
        """
        self.register_pushButton.clicked.connect(lambda: self.btn_slot('cancel'))
        self.login_pushButton.clicked.connect(lambda: self.btn_slot('login'))
        # self.login_done_signal.connect(self.handle_login)

    def btn_slot(self, tag):
        """
        Button click event slot function.
        :param tag: Tag of the button that was clicked
        :return: return if error, no subsequent operation logic will be performed
        """

        # cancel
        if tag == 'cancel':
            # close login window
            self.close()

        # ok
        if tag == 'login':
            username = self.username_lineEdit.text()
            password = self.password_lineEdit.text()
            if '' in [username, password]:
                msg_box(self, 'Prompt', 'Please input username or password!')
                return

            print("username: " + username + ",password： " + password)
            # judge that user is successfully login or not
            isuserlogin = self.moodle_controler.login(username, password, True)
            if isuserlogin:
                print("Login sucess!\n")
                # Pass the login information to the homepage
                # self.login_signal.emit(username, password, isuserlogin)
                # Enter moodle window
                url_and_json_info.control=self.moodle_controler
                new_session = newCnt(username, password)
                url_and_json_info.session=new_session
                downuser_allcourse = threading.Thread(target=down_course, args=(new_session, username,password,self))
                # Specify the task to be performed by the child thread through target.You can specify arguments to test1 with args= tuple.
                downuser_allcourse.start()
                # The child thread is created and executed only after the start method is called
                # url_and_json_info.update_time = datetime.datetime.now().strftime('%R')

                url_and_json_info.monitor_interval = url_and_json_info.user_info['monitor_interval']
                url_and_json_info.update_time = url_and_json_info.user_info['update_time']
                url_and_json_info.last_renew_time = int(time.time())
                url_and_json_info.important = []
                self.close()
            else:
                print("Login fail!\n")
                self.login_signal.emit('','',False)
                QMessageBox.about(self, "ERROR", "Username or password is incorrect！")


def down_course(session, username,password,self):
    '''
    Obtain courses information from Moodle and store courses information.
    :param session : session with Moodle server
    :param username : name of a certain course
    :param password : the password to Moodle
    '''
    print("Store all course information for this user in a folder")
    from UI.src.form_logic.FileForMac import File
    file = File()

    user_path = os.path.join(file.user_info_absolute_path, username+'_courseList')
    print(user_path)
    if os.path.exists(user_path):
        print("The user file already exists")
    else:
        print("Create the user folder")
        getList(session)
        self.moodle_controler.update_course_list()
        self.moodle_controler.scanning()

    self.login_signal.emit(username, password, True)
    print("File write completed")

