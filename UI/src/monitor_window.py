'''
author: Team 16
'''

import sys
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from UI.UI_designer.monitor_window import Ui_Monitor_Form
from UI.src.form_logic.Controller import Controller
from UI.util.common_util import *


class Monitor_Window(QWidget, Ui_Monitor_Form):
    '''
    Responsible for UI interface of Monitor Window.
    Monitor Window : UI window for selecting the courses to be monitored.
    '''
    monitor_signal = pyqtSignal()
    def __init__(self,username,passwd,parent=None):
        super(Monitor_Window, self).__init__(parent)
        self.setupUi(self)
        self.username=username
        self.passwd=passwd
        self.init_ui()
        self.init_slot()
        self.controler


    def init_ui(self):
        '''
        Initialize UI of Monitor Window.
        '''
        # init listwidget
        self.listWidget.setProperty('class', 'Normal')
        self.listWidget.setCurrentRow(0)
        # get course

        self.controler = Controller()
        self.controler.login(self.username, self.passwd, True)
        listWidget_data = self.controler.show_all_courses()

        for listWidget_item_data in listWidget_data:
            main_list_item = QListWidgetItem()
            # main_list_item.setSizeHint(QSize(260, 80))
            # QCheckBox
            course_checkbox = QCheckBox(listWidget_item_data['course_name'])
            if listWidget_item_data['monitoring']:
                course_checkbox.setChecked(True)
            else:
                course_checkbox.setChecked(False)
            self.listWidget.addItem(main_list_item)  # Add item
            self.listWidget.setItemWidget(main_list_item, course_checkbox)

        self.monitor_ptn_cancel.setProperty('class', 'Aqua')
        self.monitor_ptn_save.setProperty('class', 'Aqua')

        # Add qss effect
        self.setStyleSheet(SYS_STYLE)
        self.setWindowIcon(QIcon(APP_ICON))
        self.setWindowTitle('Monitor module')

    def init_slot(self):
        '''
        Initialize the signal slot connection.
        '''
        print("")
        self.monitor_ptn_save.clicked.connect(lambda: self.btn_slot('save'))
        self.monitor_ptn_cancel.clicked.connect(lambda:self.btn_slot('cancel'))
        # self.moodle_listWidget.currentItemChanged.connect(self.item_changed)

    def btn_slot(self,tag):
        '''
        Responsible for confirming to select the monitored courses.
        :param tag: the signal to save setting.
        '''
        if tag=='save':
            print(self.listWidget.count())
            courses_id_add=[]
            courses_id_del=[]
            for i in range(self.listWidget.count()):
                if self.listWidget.itemWidget(self.listWidget.item(i)).isChecked():
                    courses_id_add.append(i)
                else:
                    courses_id_del.append(i)
            print(courses_id_add)
            print(courses_id_del)
            self.controler.add_new_monitoring_course(courses_id_add)
            self.controler.delete_monitoring_course(courses_id_del)
            self.monitor_signal.emit()
            self.close()
        elif tag=='cancel':
            self.close()

