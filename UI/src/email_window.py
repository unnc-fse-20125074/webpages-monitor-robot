'''
author: Team 16
'''

import datetime
import sys
import time

from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from UI.UI_designer.home_window import Ui_home_Form
from UI.src.email_spider import newEmail
from UI.src.form_logic.Url_json_list import url_and_json_info
from UI.src.show_email_window import ShowEmailWindow

'''
@desc: This is the class of email window
'''


class EmailWindow(QWidget, Ui_home_Form):
    '''
    This is the class of email window
    '''
    def __init__(self, parent=None):
        '''
        Init email window
        :param parent: None
        '''
        super(EmailWindow, self).__init__(parent)
        self.setupUi(self)

    def re_draw(self, account, passwd):
        '''
        Re-init the email window
        :param account: email username
        :param passwd: email username
        :return: None
        Set renew time
        '''
        self.getEmail(account, passwd)
        self.init_ui()
        if url_and_json_info.last_renew_time_email == 0:
            url_and_json_info.last_renew_time_email = int(time.time())

    def getEmail(self, account, passwd):
        '''
        Get mails for email
        :param account: email username
        :param passwd: email username
        :return: None
        '''
        server = newEmail(account, passwd)
        # Get all emails
        self.mails = server.get_mails()

    def init_ui(self):
        '''
        Init the email window
        :return: None
        Separate different kinds of emails using tool box
        The logic is judging the time and content
        '''
        # Use toolbox to achieve the button display effect
        self.email_toolbox = QToolBox()
        self.email_toolbox.layout().setSpacing(5)
        self.scrollArea.setWidget(self.email_toolbox)
        # Content that toolbox contains
        self.data = ['Update with keywords', 'Update from important Contacts', 'Other Updates', 'History']
        # Get data

        keyword_email = []
        contants_email = []
        other_email = []
        history = []
        # Categorize email here
        for email in self.mails:
            if int(time.mktime(email['date'].timetuple())) < url_and_json_info.last_renew_time_email:
                history.append(email)
                continue
            f_k = False
            f_c = False
            for keyword in url_and_json_info.email_keywords:
                if keyword in email['subject'] or keyword in "".join(email['content_text']):
                    f_k = True
                    break
            for contact in url_and_json_info.email_contacts:
                if contact in email['from']:
                    f_c = True
                    break
            if f_k:
                keyword_email.append(email)
            if f_c:
                contants_email.append(email)
            if not f_k and not f_c:
                other_email.append(email)

        # Put each email in its own groupbox
        i = 0
        for email_item_data in self.data:
            self.email_groupbox = QGroupBox()
            # self.email_groupbox.setFixedSize(600, 180)
            self.email_groupbox_layout = QVBoxLayout()
            self.email_groupbox_layout.setAlignment(Qt.AlignCenter)
            self.email_groupbox.setLayout(self.email_groupbox_layout)

            if i == 0:
                self.temp_email = keyword_email
            elif i == 1:
                self.temp_email = contants_email
            elif i == 2:
                self.temp_email = other_email
            else:
                self.temp_email = history

            for email in self.temp_email:
                # Words on the button are meant to convey information
                self.button_text = str(email['id']) + ' ' + email['subject'] + ' ' + email[
                    'from'] + ' ' + datetime.datetime.strftime(email['date'], "%Y-%m-%d %H:%M:%S")
                self.moodle_btn = QPushButton(self.button_text)
                self.delete_btn = QPushButton('delete' + ' ' + str(email['id']))
                # self.moodle_btn.setText(email['from'])
                # self.moodle_btn.setFixedSize(500, 40)
                self.moodle_btn.setProperty('class', 'Email_button')
                self.delete_btn.setProperty('class', 'Email_button_2')
                b = QHBoxLayout()
                b.addWidget(self.moodle_btn)
                b.addWidget(self.delete_btn)
                self.moodle_btn.clicked.connect(lambda: self.btn_email(self.sender().text()))
                self.delete_btn.clicked.connect(lambda: self.btn_delete_email(self.sender().text()))

                # self.email_groupbox_layout.addWidget(self.moodle_btn)
                self.email_groupbox_layout.addLayout(b)
            i += 1
            self.email_toolbox.addItem(self.email_groupbox, email_item_data)

    def btn_delete_email(self, text):
        '''
        Delete the email depending the id
        :param text: button name
        :return: None
        '''
        text = text.split(' ')[1]
        print('delete' + str(text))
        url_and_json_info.server.delete(int(text))
        self.re_draw('','')

    # Call email window
    def btn_email(self, email_text):
        '''
        Show email detail
        :param email_text:
        :return: None
        Find the email with id
        '''
        email_index = email_text.split(' ')[0]
        for email in self.mails:
            if email_index == str(email['id']):
                self.showemail_window = ShowEmailWindow()
                self.showemail_window.announcelabel.setText(email['subject'])
                self.showemail_window.anntimelabel.setText(email['from'])
                self.showemail_window.announcetextEdit.setText(str(email['content_text']))
                self.showemail_window.show()


if __name__ == "__main__":
    app = QApplication(sys.argv)
    myWin = EmailWindow()
    myWin.show()
    sys.exit(app.exec_())
