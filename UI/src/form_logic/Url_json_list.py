'''
author: Team 16
'''

class url_and_json_info():

    email_keywords=[]
    email_contacts=[]
    server=''


    session=''

    user_info=[]

    email_info = []

    save_path=''

    control=''

    update_time='8:00'

    last_renew_time_email=0
    last_renew_time=0

    monitor_interval='1'

    root=''
    root_os=''
    download_path=''

    important=[]

    login_request_url = "https://moodle.nottingham.ac.uk/login/index.php"
    home_page_url = "https://moodle.nottingham.ac.uk/my/"
    log_out_request_url = "https://moodle.nottingham.ac.uk/login/logout.php?sesskey="
    folder_download_request_url = "https://moodle.nottingham.ac.uk/mod/folder/download_folder.php"
    course_list_request_url_1 = "https://moodle.nottingham.ac.uk/lib/ajax/service.php?sesskey="
    course_list_request_url_2 = "&info=core_course_get_enrolled_courses_by_timeline_classification"


    # Json list need in other class

    all_course_list_json = [{"index": 0, "methodname": "core_course_get_enrolled_courses_by_timeline_classification",
                             "args": {"offset": 0, "limit": 0, "classification": "all", "sort": "fullname",
                                      "customfieldname": "", "customfieldvalue": ""}}]
    stared_course_list_json = [{"index": 0, "methodname": "core_course_get_enrolled_courses_by_timeline_classification",
                                "args": {"offset": 0, "limit": 0, "classification": "favourites", "sort": "fullname",
                                         "customfieldname": "", "customfieldvalue": ""}}]


