'''
author: Team 16
'''

import requests
from bs4 import BeautifulSoup
from UI.src.form_logic.Url_json_list import url_and_json_info


class Moodle:
    # Save username, password and other user info
    passData = {}
    # Sesskey in Moodle after login
    sesskey = None
    # Session for keeping connection with Moodle server
    session = None

    # Init param in Moodle
    def __init__(self):
        self.passData["username"] = ""
        self.passData["password"] = ""
        self.passData["anchor"] = ""
        self.passData["logintoken"] = ""

        self.session = requests.session()
        self.__update_token()

    # Input username and password to simulate login
    def login(self, username, password):
        self.set_username_and_password(username, password)
        if self.re_login():
            self.__update_sesskey()
            return True
        else:
            return False

    # Reset username and password
    def set_username_and_password(self, username, password):
        self.passData['username'] = username
        self.passData['password'] = password
        return True


    def re_login(self):
        if not self.__update_token():
            return False
        login = url_and_json_info.login_request_url
        html = self.session.post(login, data=self.passData).text
        bsObj = BeautifulSoup(html, 'lxml')
        if bsObj.find("title").text == "Dashboard":
            return True
        else:
            print("Fail to login, error code: 101")
            return False

    def log_out(self):
        log_out = url_and_json_info.log_out_request_url + str(self.sesskey)
        # print(log_out)
        self.session.get(log_out)
        if self.sesskey is not None:
            return True
        else:
            return False

    def test_connect(self):
        home_page = url_and_json_info.home_page_url
        html = self.session.get(home_page).text
        bsObj = BeautifulSoup(html, 'lxml')
        if bsObj.find("title").text == "Dashboard":
            return True
        else:
            return False

    def __update_token(self):
        login = url_and_json_info.login_request_url

        print(login)
        login_page_html = self.session.get(login)
        bsObj = BeautifulSoup(login_page_html.text, "lxml")  # need install lxml (> pip install lxml)
        if (bsObj.find("form", {'class': 'loginform'})) is not None:
            token = bsObj.find("form", {'class': 'loginform'}).find("input", {'name': 'logintoken'}).attrs['value']
        else:
            print("You has login, error code: 102")
            return False
        self.passData['logintoken'] = token
        return True

    def __update_sesskey(self):
        HomeUrl = url_and_json_info.home_page_url

        home_page_html = self.session.get(HomeUrl)
        bsObj = BeautifulSoup(home_page_html.text, 'lxml')
        if bsObj.find("input", {'name': 'sesskey'}) is not None:
            self.sesskey = bsObj.find("input", {'name': 'sesskey'}).attrs['value']
            return True
        else:
            print("Can not get sesskey, error code: 103")
            return False

