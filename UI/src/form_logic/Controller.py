'''
author: Team 16
'''
from UI.src.form_logic.FileForMac import File
from UI.src.form_logic.Moodle import Moodle
from UI.src.form_logic.MoodleCrawler import MoodleCrawler
from UI.src.form_logic.Url_json_list import url_and_json_info


class Controller:
    test = True
    moodle = None
    moodle_crawler = None
    file = None
    if_save_username_and_password = False
    user_data_update = []
    user_courseList_info = []
    user_course_info = []
    user_forum_info = {}
    forum_ignore_list = {}
    __passData = None

    def __init__(self):
        self.moodle = Moodle()
        self.moodle_crawler = MoodleCrawler(self.moodle)
        self.file = File()

    def ignore_init(self):
        forum_ignore_list = self.file.loadJson(self.file.user_info_absolute_path +
                                               self.__passData['username'] + "_ignoreList")
        if forum_ignore_list:
            self.forum_ignore_list = forum_ignore_list

    def login(self, username, password, save):
        if_success = self.moodle.login(username, password)
        if if_success is False:
            return False
        self.__passData = self.moodle.passData
        self.if_save_username_and_password = save
        self.save_user_data()
        self.show_monitoring_course()
        self.init_user_course_info()
        self.ignore_init()
        return if_success

    def scanning(self):
        self.user_data_update = []

        past_courses_info = self.file.loadJson(self.file.user_info_absolute_path +
                                               self.__passData['username'] + "_course")
        user_info = self.show_all_courses()

        current_course_info = []
        if not past_courses_info:
            past_courses_info = []

        for i in range(len(user_info)):
            if user_info[i]['monitoring']:
                course_name = user_info[i]['course_name']

                past_course_info = {}

                for j in range(len(past_courses_info)):
                    if range(len(past_courses_info)) == 0:
                        break
                    if past_courses_info[j].get('Name') == course_name:
                        past_course_info = past_courses_info[j]
                        break
                current_course_info.append(
                    self.course_compare(past_course_info, user_info[i]['course_name'], user_info[i]['course_url']))

        self.file.saveJson(self.file.user_info_path, current_course_info, self.__passData['username'] + "_course")
        return self.user_data_update

    # def get_forum_info(self, course_name, url):
    def forum_adjusting(self):
        forum_update = []
        ignore_forum_update = []
        forum_history = []
        ignore_forum_history = []
        all_forum_update = self.user_forum_info['all_forum_update']
        all_forum_history = self.user_forum_info['all_forum_history']
        # print(self.user_forum_info['forum_update'])
        # print(self.user_forum_info['ignore_forum_update'])
        # print(self.user_forum_info['forum_history'])
        # print(self.user_forum_info['ignore_forum_history'])

        for forum in all_forum_update:
            important_forum = {'forum_name': forum['forum_name'], 'forum_url': forum['forum_url'], 'forum_data': []}
            unimportant_forum = {'forum_name': forum['forum_name'], 'forum_url': forum['forum_url'], 'forum_data': []}
            for discuss in forum['forum_data']:
                if forum['forum_url'] in self.forum_ignore_list and \
                        discuss['discuss_url'] in self.forum_ignore_list.get(forum['forum_url']):
                    unimportant_forum['forum_data'].append(discuss)
                else:
                    important_forum['forum_data'].append(discuss)

            forum_update.append(important_forum)
            ignore_forum_update.append(unimportant_forum)
        self.user_forum_info['forum_update'] = forum_update
        self.user_forum_info['ignore_forum_update'] = ignore_forum_update

        for forum in all_forum_history:
            important_forum = {'forum_name': forum['forum_name'], 'forum_url': forum['forum_url'], 'forum_data': []}
            unimportant_forum = {'forum_name': forum['forum_name'], 'forum_url': forum['forum_url'], 'forum_data': []}
            for discuss in forum['forum_data']:
                if forum['forum_url'] in self.forum_ignore_list and \
                        discuss['discuss_url'] in self.forum_ignore_list.get(forum['forum_url']):
                    unimportant_forum['forum_data'].append(discuss)
                else:
                    important_forum['forum_data'].append(discuss)

            forum_history.append(important_forum)
            ignore_forum_history.append(unimportant_forum)
        self.user_forum_info['forum_history'] = forum_history
        self.user_forum_info['ignore_forum_history'] = ignore_forum_history

        # print(self.user_forum_info['forum_update'])
        # print(self.user_forum_info['ignore_forum_update'])
        # print(self.user_forum_info['forum_history'])
        # print(self.user_forum_info['ignore_forum_history'])

    def forum_scan(self, url):

        print("Start to scan courses now：" + url)
        self.moodle_crawler.set_current_page(url)
        current_forum = self.moodle_crawler.get_forum()

        course_info = self.user_course_info

        forum_info = []
        course_name = None
        exite = False
        forum_exite = True
        i = int
        for i in range(len(course_info)):
            if course_info[i].get("url") == url:
                course_name = course_info[i].get("Name")
                forum_info = course_info[i].get("forum")
                exite = True
                break

        if len(forum_info) == 0:
            forum_exite = False
            for forum in current_forum:
                temp = {'forum_name': forum['forum_name'], 'forum_url': forum['forum_url'], 'forum_data': []}
                forum_info.append(temp)

        forum_update, ignore_forum_update = self.compare_forum(forum_info, current_forum)

        if forum_exite is False:
            forum_info = current_forum

        forum_history = []
        ignore_forum_history = []
        for forum in forum_info:
            important_forum = {'forum_name': forum['forum_name'], 'forum_url': forum['forum_url'], 'forum_data': []}
            unimportant_forum = {'forum_name': forum['forum_name'], 'forum_url': forum['forum_url'], 'forum_data': []}
            for discuss in forum['forum_data']:
                if forum['forum_url'] in self.forum_ignore_list and \
                        discuss['discuss_url'] in self.forum_ignore_list.get(forum['forum_url']):
                    unimportant_forum['forum_data'].append(discuss)
                else:
                    important_forum['forum_data'].append(discuss)

            forum_history.append(important_forum)
            ignore_forum_history.append(unimportant_forum)

        if not exite:
            temp = {'Name': course_name, 'url': url, 'folder': [], 'inner_link': [],
                    'outer_link': [], 'forum': []}
            course_info.append(temp)
        else:
            course_info[i]['forum'] = current_forum
        self.user_course_info = course_info
        self.file.saveJson(self.file.user_info_path, self.user_course_info, self.__passData['username'] + "_course")

        temp = {'forum_update': forum_update, 'ignore_forum_update': ignore_forum_update,
                'forum_history': forum_history, 'ignore_forum_history': ignore_forum_history,
                'all_forum_update': forum_update, 'all_forum_history': forum_info}
        self.user_forum_info = temp
        # print(self.user_forum_info['forum_update'])
        # print(self.user_forum_info['ignore_forum_update'])
        # print(self.user_forum_info['forum_history'])
        # print(self.user_forum_info['ignore_forum_history'])
        return forum_update, ignore_forum_update, forum_history, ignore_forum_history, current_forum, forum_info

    def scan_course(self, course_name, url):
        print("Start to scan courses now：" + course_name)
        self.moodle_crawler.set_current_page(url)
        folder = self.moodle_crawler.get_folder()
        inner_link = self.moodle_crawler.get_inner_link()
        outer_link = self.moodle_crawler.get_outer_link()
        forum = self.moodle_crawler.get_forum()

        course_info = {'Name': course_name, 'url': url, 'folder': folder, 'inner_link': inner_link,
                       'outer_link': outer_link, 'forum': forum}

        return course_info

    def course_compare(self, past_course_info, course_name, url):

        current_course_info = self.scan_course(course_name, url)

        if not current_course_info:
            return False

        folder_update = self.compare_list(past_course_info.get('folder'), current_course_info['folder'])
        inner_link_update = self.compare_list(past_course_info.get('inner_link'), current_course_info['inner_link'])
        outer_link_update = self.compare_list(past_course_info.get('outer_link'), current_course_info['outer_link'])
        try:
            forum_update, temp = self.compare_forum(past_course_info.get('forum'), current_course_info['forum'])
        except:
            forum_update = self.compare_forum(past_course_info.get('forum'), current_course_info['forum'])

        course_update = {'Name': course_name, 'url': url, 'folder': folder_update, 'inner_link': inner_link_update,
                         'outer_link': outer_link_update, 'forum': forum_update}

        self.user_data_update.append(course_update)
        return current_course_info

    def compare_list(self, past_course_list, current_course_list):
        list_update = []

        if past_course_list is None:
            return current_course_list

        for element in current_course_list:
            if element not in past_course_list:
                if element not in list_update:
                    list_update.append(element)
        return list_update

    def compare_forum(self, past_course_forum, current_course_forum):
        forum_update = []
        ignore_forum_update = []
        if past_course_forum is None:
            return current_course_forum
        for current_forum in current_course_forum:
            flag = False
            for past_forum in past_course_forum:
                if current_forum['forum_url'] == past_forum['forum_url']:
                    flag = True
                    break
            if flag is False:
                forum_update.append(current_forum)
                print("New forum！")

        for current_forum in current_course_forum:
            flag = True
            ignore_flag = True
            forum_discuss_update = {}
            ignore_forum_discuss_update = {}
            for past_forum in past_course_forum:
                if current_forum['forum_url'] == past_forum['forum_url']:
                    forum_discuss_update['forum_name'] = current_forum['forum_name']
                    forum_discuss_update['forum_url'] = current_forum['forum_url']
                    forum_discuss_update['forum_data'] = []
                    ignore_forum_discuss_update = forum_discuss_update
                    for discuss in current_forum['forum_data']:
                        if discuss not in past_forum['forum_data']:
                            if current_forum['forum_url'] in self.forum_ignore_list and \
                                    discuss['discuss_url'] in self.forum_ignore_list.get(current_forum['forum_url']):
                                ignore_forum_discuss_update['forum_data'].append(discuss)
                                ignore_flag = True
                                continue

                            flag = True
                            forum_discuss_update['forum_data'].append(discuss)
            if flag:
                forum_update.append(forum_discuss_update)
            if ignore_flag:
                ignore_forum_update.append(ignore_forum_discuss_update)
        # https://www.bejson.com/jsonviewernew/

        return forum_update, ignore_forum_update

    def init_user_course_info(self):
        self.user_course_info = self.file.loadJson(self.file.user_info_absolute_path +
                                                   self.__passData['username'] + "_course")
        user_info = self.user_courseList_info
        if not self.user_course_info:
            user_course_info = []
            for i in range(len(user_info)):
                if user_info[i]['monitoring']:
                    course_name = user_info[i]['course_name']
                    url = user_info[i]['course_url']
                    temp = {'Name': course_name, 'url': url, 'folder': [], 'inner_link': [],
                            'outer_link': [], 'forum': []}
                    user_course_info.append(temp)
            self.user_course_info = user_course_info
        else:
            user_course_info = self.user_course_info
            for i in range(len(user_info)):
                if user_info[i]['monitoring']:
                    course_name = user_info[i]['course_name']
                    url = user_info[i]['course_url']
                    flag = False
                    for j in range(len(user_course_info)):
                        if user_course_info[j].get("Name") == course_name:
                            flag = True
                            break
                    if not flag:
                        temp = {'Name': course_name, 'url': url, 'folder': [], 'inner_link': [],
                                'outer_link': [], 'forum': []}
                        user_course_info.append(temp)
            self.user_course_info = user_course_info
        self.file.saveJson(self.file.user_info_path, self.user_course_info, self.__passData['username'] + "_course")

    def update_course_list(self):
        new_list = self.moodle_crawler.getList(True)
        for i in range(len(new_list)):
            new_list[i]['monitoring'] = False
            new_list[i]['id'] = i
        self.user_courseList_info = new_list
        self.file.saveJson(self.file.user_info_path, new_list, self.__passData['username'] + "_courseList")
        return new_list

    def show_all_courses(self):
        course_list = self.file.loadJson(self.file.user_info_absolute_path +
                                         self.__passData['username'] + "_courseList")
        if not course_list:
            course_list = self.update_course_list()

        if self.test:
            for i in range(len(course_list)):
                print(course_list[i]['id'])
                print(course_list[i]['course_name'])
                print(course_list[i]['course_url'])
                print(course_list[i]['monitoring'])

        return course_list

    def add_new_monitoring_course(self, courses_id):
        old_list = self.file.loadJson(self.file.user_info_absolute_path +
                                      self.__passData['username'] + "_courseList")
        print(courses_id)
        print(old_list)
        for course_id in courses_id:
            old_list[int(course_id)]['monitoring'] = True
        print(old_list)

        course_list = old_list
        self.file.saveJson(self.file.user_info_path, course_list, self.__passData['username'] + "_courseList")
        return course_list

    def delete_monitoring_course(self, courses_id):
        old_list = self.file.loadJson(self.file.user_info_absolute_path +
                                      self.__passData['username'] + "_courseList")
        if not old_list:
            return

        for course_id in courses_id:
            old_list[int(course_id)]['monitoring'] = False

        course_list = old_list
        self.file.saveJson(self.file.user_info_path, course_list, self.__passData['username'] + "_courseList")
        return course_list

    def show_monitoring_course(self):
        course_list = self.file.loadJson(self.file.user_info_absolute_path +
                                         self.__passData['username'] + "_courseList")

        if not course_list:
            course_list = self.update_course_list()

        monitoring_course_list = []
        for course_id in range(len(course_list)):
            if course_list[int(course_id)]['monitoring']:
                print(course_list[int(course_id)]['course_name'])
                monitoring_course_list.append(course_list[int(course_id)]['id'])
        self.user_courseList_info = course_list
        return monitoring_course_list

    def forum_ignore(self, forum_url, discuss_url):
        forum_ignore_list = self.file.loadJson(self.file.user_info_absolute_path +
                                               self.__passData['username'] + "_ignoreList")

        if forum_ignore_list:
            self.forum_ignore_list = forum_ignore_list
        if self.forum_ignore_list.get(forum_url) is None:
            self.forum_ignore_list[forum_url] = []
        if discuss_url not in self.forum_ignore_list.get(forum_url):
            self.forum_ignore_list[forum_url].append(discuss_url)
        try:
            self.forum_adjusting()
        except:
            print("Controller.forum_ignore")
        self.file.saveJson(self.file.user_info_path, self.forum_ignore_list,
                           self.__passData['username'] + "_ignoreList")

    def remove_forum_ignore(self, forum_url, discuss_url):
        self.forum_ignore_list = self.file.loadJson(self.file.user_info_absolute_path +
                                                    self.__passData['username'] + "_ignoreList")

        if not self.forum_ignore_list:
            self.forum_ignore_list[forum_url] = []

        if self.forum_ignore_list.get(forum_url) is None:
            self.forum_ignore_list[forum_url] = []

        for key in self.forum_ignore_list.keys():
            if key == forum_url:
                value = self.forum_ignore_list.get(key)
                if discuss_url in value:
                    value.remove(discuss_url)
                    self.forum_ignore_list[key] = value
                    break

        try:
            self.forum_adjusting()
        except:
            print("Controller.remove_forum_ignore")
        self.file.saveJson(self.file.user_info_path, self.forum_ignore_list,
                           self.__passData['username'] + "_ignoreList")

    def show_ignore(self):
        return self.forum_ignore_list

    def re_login(self):
        return self.moodle.re_login()

    def log_out(self):
        return self.moodle.log_out()

    def test_connect(self):
        return self.moodle.test_connect()

    def save_user_data(self):
        user_info = {'username': self.__passData['username']}
        if self.if_save_username_and_password is False:
            user_info['password'] = ''
        else:
            user_info['password'] = self.__passData['password']
        user_info['logintoken'] = ''
        user_info['anchor'] = ''
        user_info['monitor_interval']='1'
        user_info['update_time']='8:00'
        user_info['download_path'] = url_and_json_info.download_path

        test = self.file.loadJson(self.file.user_info_absolute_path +
                                  user_info['username'])
        print(self.file.user_info_path + user_info['username'])

        if not test:
            self.file.saveJson(self.file.user_info_path, user_info, user_info['username'])
            url_and_json_info.user_info = user_info
        else:
            url_and_json_info.user_info = test

        url_and_json_info.save_path = self.file.user_info_path


