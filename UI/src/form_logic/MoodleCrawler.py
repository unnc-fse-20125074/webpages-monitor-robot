'''
author: Team 16
'''

import re

from bs4 import BeautifulSoup
from UI.src.form_logic.Moodle import Moodle
from UI.src.form_logic.Url_json_list import url_and_json_info


class MoodleCrawler:
    moodle = None
    __page_html = None
    __bsObj = None

    def __init__(self, moodle):
        self.moodle = moodle

    def set_current_page(self, url):
        self.__page_html = self.moodle.session.get(url).text
        self.__bsObj = BeautifulSoup(self.__page_html, 'lxml')

    def get_folder(self):
        Folder = []
        temp = {}
        bsObj = self.__bsObj
        # print(bsObj.decode('utf-8'))
        folderUrl = bsObj.findAll("a", href=re.compile(".*((/mod/)(folder))"))
        print("There are " + str(len(folderUrl)) + " Folder in this web")
        for link in folderUrl:
            if 'href' in link.attrs:
                if 'instancename' in link.span.attrs['class']:
                    temp = {}
                    temp['folder_name'] = link.span.text
                    temp['folder_url'] = link.attrs['href']
                    Folder.append(temp)
        return Folder


    def get_inner_link(self):
        inner_link = []
        bsObj = self.__bsObj
        # print(bsObj.decode('utf-8'))
        inner_link_url = bsObj.findAll("a", href=re.compile(".*((/mod/)[(resource)(url)])"))
        print("There are " + str(len(inner_link_url)) + " inner link in this web")
        for link in inner_link_url:
            if 'href' in link.attrs:
                if 'instancename' in link.span.attrs['class']:
                    temp = {}
                    temp['inner_link_name'] = link.span.text
                    temp['inner_link_url'] = link.attrs['href']
                    temp['inner_link_downloadable'] = False
                    temp['inner_link_download_url'] = []
                    img = link.find("img", src=re.compile(".*(/f/)"))
                    if img is not None:
                        temp['inner_link_downloadable'] = True
                    if temp['inner_link_downloadable']:
                        None
                        # download_page = self.moodle.session.get(temp['inner_link_url']).text
                        # bsObj = BeautifulSoup(download_page, 'lxml')
                        # inner_link_download_url = bsObj.findAll("a", href=re.compile(".*(pluginfile)"))
                        # for i in inner_link_download_url:
                        #     temp['inner_link_download_url'].append(i.attrs['href'])
                    inner_link.append(temp)
        return inner_link


    def get_outer_link(self):
        outer_link = []
        bsObj = self.__bsObj
        outer_link_url = bsObj.findAll("a", href=re.compile(
            "^((?!nottingham)(?!moodle).)*http((?!nottingham)(?!moodle).)*$"))
        print("There are " + str(len(outer_link_url)) + " outer link in this web")
        for link in outer_link_url:
            temp = {}
            temp['outer_link_name'] = link.text
            temp['outer_link_url'] = link.attrs['href']
            outer_link.append(temp)
        return outer_link

    def get_forum_data(self, url):
        forum = []
        page_html = self.moodle.session.get(url).text
        bsObj = BeautifulSoup(page_html, 'lxml')
        forum_data = bsObj.findAll("tr", {'data-region': 'discussion-list-item'})
        for link in forum_data:
            temp = {}
            data = link.find("th", attrs={'scope': 'row'}).div.a
            temp['discuss_title'] = data.attrs['title']
            temp['discuss_url'] = data.attrs['href']
            temp['discuss_start_time'] = \
                link.find("td", attrs={'class': 'author align-middle fit-content limit-width px-3'}).div.find("div",
                                                                                                              attrs={
                                                                                                                  'class': 'author-info align-middle'}).findAll(
                    "div", attrs={'class': 'line-height-3'}, )[1].time.attrs[
                    'data-timestamp']

            temp['discuss_last_post_time'] = \
                link.find("td", attrs={'class': 'text-left align-middle fit-content limit-width px-3'}).div.find("div",
                                                                                                                 attrs={
                                                                                                                     'class': 'author-info align-middle'}).findAll(
                    "div", attrs={'class': 'line-height-3'}, )[1].a.time.attrs[
                    'data-timestamp']
            forum.append(temp)
        return forum

    # Warning： 在 set_current_page() 后运行
    # 获取网页中所有讨论组，储存讨论组名字和链接，并调用 get_forum_data， 将返回值储存
    def get_forum(self):
        forum = []
        bsObj = self.__bsObj
        forum_link = bsObj.find("ul", attrs={'class': 'topics'}).findAll("a", href=re.compile(".*((/mod/)(forum))"))
        print("There are " + str(len(forum_link)) + " forum in this web")
        for link in forum_link:
            if 'href' in link.attrs:
                if 'instancename' in link.span.attrs['class']:
                    temp = {}
                    temp['forum_name'] = link.span.text
                    temp['forum_url'] = link.attrs['href']
                    temp['forum_data'] = self.get_forum_data(temp['forum_url'])
                    forum.append(temp)
        return forum

    def getList(self, all):

        if all:
            courseListJson = url_and_json_info.all_course_list_json
        elif not all:
            courseListJson = url_and_json_info.stared_course_list_json
        else:
            courseListJson = url_and_json_info.all_course_list_json

        jsonRqtUrl = url_and_json_info.course_list_request_url_1 + self.moodle.sesskey + url_and_json_info.course_list_request_url_2

        courseJson = self.moodle.session.post(jsonRqtUrl, json=courseListJson)
        if courseJson.status_code == 200:
            rep = courseJson.json()
            return self.__data_cleanse(rep)
        return False

    def __data_cleanse(self, courseJson):
        list_data = []
        courseJson = courseJson[0]['data']['courses']
        for i in range(len(courseJson)):
            temp = {}
            temp['course_name'] = courseJson[i]['fullname']
            temp['course_url'] = courseJson[i]['viewurl']
            list_data.append(temp)
        return list_data

