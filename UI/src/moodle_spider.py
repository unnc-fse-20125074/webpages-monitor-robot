'''
author: Team 16
'''

'''
@desc: This module is responsible for crawling Moodle，including simulating login Moodle, obtaining user's courses,
obtaining the updated data of forum and announcement and downloading updated courseware.
'''

import os
import re
import requests
from bs4 import BeautifulSoup
from UI.src.form_logic.Url_json_list import url_and_json_info


def newCnt(username, password):
    '''
    Simulate login Moodle and maintain the current session.
    :param username : the username to Moodle
    :param password : the password to Moodle
    :return session : the object that is the current session with Moodle server
    '''
    if url_and_json_info.session!='':
        return url_and_json_info.session
    passData = {}
    passData["username"] = username
    passData["password"] = password
    passData["anchor"] = ""
    passData["logintoken"] = ""
    login_url = "https://moodle.nottingham.ac.uk/login/index.php"
    # create session object and access login
    session = requests.session()
    login_html = session.get(login_url)
    try:
        # Get all the information of the page, query token to put the value into passData
        soup = BeautifulSoup(login_html.text, 'lxml')
        login_token_mess = soup.find(attrs={'name':'logintoken'})
        login_token_value = login_token_mess.attrs['value']
        passData["logintoken"] = login_token_value
    except:
        print("Error: moodle spider get session error")
        return

    # Pass the obtained login information to the session server
    login_OK = session.post(login_url, data=passData)
    return session


def getList(session):
    '''
    Obtain the course list from Moodle homepage.
    :param session : session with Moodle server
    :return coursedata : courses list with single course's name and url
    '''
    HomeUrl = "https://moodle.nottingham.ac.uk/my/"
    # Request to print the entire course's json
    courseListJson = [{"index": 0, "methodname": "core_course_get_enrolled_courses_by_timeline_classification",
                       "args": {"offset": 0, "limit": 0, "classification": "all", "sort": "fullname", "customfieldname": "", "customfieldvalue": ""}}]

    # The json address is divided into two parts,
    # one part of the sesskey that needs to be fetched before printing,
    # and the other part of the info that is fixed
    jsonRqtUrl_1 = "https://moodle.nottingham.ac.uk/lib/ajax/service.php?sesskey="
    jsonRqtUrl_2 = "&info=core_course_get_enrolled_courses_by_timeline_classification"

    homepage = session.get(HomeUrl)
    homepage_soup = BeautifulSoup(homepage.text, 'lxml')
    homepage_soup_sesskey = homepage_soup.find('a',attrs={'data-title':'logout,moodle'})
    homepage_soup_sesskey_url = homepage_soup_sesskey.attrs['href']
    sekey = str(homepage_soup_sesskey_url).find("sesskey=")
    sesskey = str(homepage_soup_sesskey_url)[sekey + 8:sekey + 18]
    jsonRqtUrl = jsonRqtUrl_1 + sesskey + jsonRqtUrl_2
    # Request the json for the course list
    courseJson = session.post(jsonRqtUrl, json=courseListJson)
    if courseJson.status_code == 200:
        rep = courseJson.json()
    # Clean datd
    coursedata = dataCleanse(rep)
    # coursedatajson = json.dumps(coursedata)
    return coursedata


def dataCleanse(courseJson):
    '''
    Parse the data of the course list in json format and store courses' names and urls in list.
    :param courseJson : course list obtained from Moodle in json format
    :return courseitem_list : courses list with single course's name and url
    '''
    courseJson = courseJson[0]['data']['courses']
    courseitem_list = []
    for i in range(len(courseJson)):
        courseName = courseJson[i]['fullname']
        courseUrl = courseJson[i]['viewurl']
        courseitem_list.append({"courseName": courseName, "courseUrl": courseUrl})
    return courseitem_list


# Download courseware to the specified folder
def urlAnalysis(session, name, html,dir):
    '''
    Download updated courseware.
    :param session : session with Moodle server
    :param name : name of a certain course
    :param html : url of the homepage of a certain course on Moodle
    :param dir : path to store courseware of a certain course
    :return Folder : list to store courseware information
    '''
    cookiesjar = session.cookies
    cookies = requests.utils.dict_from_cookiejar(cookiesjar)
    req = requests
    if not os.path.exists(os.path.join(dir,name)):
        os.makedirs(os.path.join(dir,name))
    ppath=os.path.join(dir,name)
    file_list = os.listdir(os.path.join(os.getcwd()))

    # Get folder urls
    folder_html = req.get(html, cookies=cookies)
    Folder = {}

    folder_soup = BeautifulSoup(folder_html.text, 'lxml')
    folderUrl = folder_soup.findAll("a", href=re.compile(".*((/mod/)(folder))"))
    InnerUrl = folder_soup.findAll("a",href =re.compile(".*((/mod/)(resource/)(view))"))
    print(InnerUrl)

    fileUrl = folder_soup.findAll("a", href=re.compile('.*\/pluginfile.php.*'))
    print(fileUrl)

    print("There are " + str(len(folderUrl)) + " Folder,"
          + str(len(InnerUrl))+ " Innerlink,"
          + str(len(fileUrl)) + " files in the web")

    if len(fileUrl):
        for a_list in fileUrl:
            # print(a_list)
            href = a_list.attrs['href']
            file_name = str(href).split('/')[-1]
            if file_name.find('?') != -1:
                file_name = file_name[0:file_name.find('?')]
            if file_name not in file_list:
                try:
                    if os.path.exists(ppath + '/'+ file_name):
                        continue
                except:
                    None
                print("Download")
                print(file_name)
                # try:
                pdf_down = req.get(href, stream="TRUE", cookies=cookies)
                # except:
                #     continue
                with open(os.path.join(ppath, file_name), "wb") as Pypdf:
                    for chunk in pdf_down.iter_content(chunk_size=1024):  # 1024 bytes
                        if chunk:
                            Pypdf.write(chunk)

        print('finish downloading direct courseware in page.')

    if (len(InnerUrl)):
        for i in range(len(InnerUrl)):
            a_link = InnerUrl[i]
            # Download pdf
            detail_html = req.get(a_link.attrs['href'], cookies=cookies)
            bsObj = BeautifulSoup(detail_html.text, 'lxml')
            inner_down_a = bsObj.findAll("a", href=re.compile(".*\/pluginfile\.php\/\d+\/mod_resource"))
            for a in inner_down_a:
                if 'href' in a.attrs:
                    if a.text not in file_list:
                        try:
                            if os.path.exists(ppath + "/" + a.text):
                                continue
                        except:
                            None
                        print("download")
                        print(a.text)
                        # try:
                        pdf_down = req.get(a.attrs['href'], stream="TRUE", cookies=cookies)
                        # except:
                        #     continue
                        with open(os.path.join(ppath, a.text), "wb") as Pypdf:
                            for chunk in pdf_down.iter_content(chunk_size=1024):  # 1024 bytes
                                if chunk:
                                    Pypdf.write(chunk)
        print("finish downloading courseware in innerlink")

    # Download courseware in folders
    if len(folderUrl):
        for link in folderUrl:
            if 'href' in link.attrs:
                if 'instancename' in link.span.attrs['class']:
                    Folder[link.span.text] = link.attrs['href']
                    # Download pdf
                    pdf_down_html = req.get(link.attrs['href'], cookies=cookies)
                    pdf_down_soup = BeautifulSoup(pdf_down_html.text, 'lxml')
                    pdf_down_url = pdf_down_soup.findAll("a", href=re.compile(".*(mod_folder)"))
                    for link in pdf_down_url:
                        if 'href' in link.attrs:
                            if link.text not in file_list:
                                try:
                                    if os.path.exists(ppath +"/" + link.text):
                                        continue
                                except:
                                    None
                                print("download")
                                print(link.text)
                                # try:
                                pdf_down = req.get(link.attrs['href'], stream="TRUE", cookies=cookies)
                                # except:
                                #     continue
                                with open(os.path.join(ppath,link.text), "wb") as Pypdf:
                                    for chunk in pdf_down.iter_content(chunk_size=1024):  # 1024 bytes
                                        if chunk:
                                            Pypdf.write(chunk)
        print("finish downloading courseware in folder")