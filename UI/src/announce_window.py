'''
author: Team 16
'''

import sys
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from UI.UI_designer.announce_window import *
from UI.util.common_util import APP_ICON, SYS_STYLE

"""
@desc: This is the design of UI for monitor window
"""

class AnnounceWindow(QWidget, Ui_AnnounceForm):
    """
    This is the class of announcement window
    """
    def __init__(self, parent=None):
        '''
        This is the constructor of AnnounceWindow
        :param parent: None
        Call setupUi function
        Call init_ui function
        '''
        super(AnnounceWindow, self).__init__(parent)
        self.setupUi(self)
        self.init_ui()

    def init_ui(self):
        '''
        Initialize the interface UI elements
        :return: None
        Set the window style
        '''
        self.setWindowTitle('Announcements')
        self.setWindowIcon(QIcon(APP_ICON))
        self.setStyleSheet(SYS_STYLE)
        self.setWindowFlags(Qt.WindowCloseButtonHint|Qt.WindowMinimizeButtonHint)

if __name__ == "__main__":
    app = QApplication(sys.argv)
    myWin = AnnounceWindow()
    myWin.show()
    sys.exit(app.exec_())

