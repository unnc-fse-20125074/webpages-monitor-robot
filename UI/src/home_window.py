'''
author: Team 16
'''

from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from UI.UI_designer.home_window import Ui_home_Form
from UI.src.login_window import LoginWindow
from UI.util.common_util import *


'''
@desc: This is the class for home window
'''

class HomeWindow(QWidget, Ui_home_Form):
    '''
    This is the class for home window
    '''
    # create a signal judge that login is successful or not
    home_login_signal = pyqtSignal(str, str, bool)
    # customized signal
    home_email_signal = pyqtSignal(str)
    home_setting_signal = pyqtSignal()
    def __init__(self, username, userpassword, isuserlogin, parent=None):
        '''
        Init param
        :param username: moodle username
        :param userpassword: moodle password
        :param isuserlogin: whether moodle login
        :param parent: None
        '''
        super(HomeWindow, self).__init__(parent)
        self.setupUi(self)
        self.init_ui()
        self.username = username
        self.userpassword = userpassword
        self.isuserlogin = isuserlogin
    def init_ui(self):
        '''
        Set home page ui
        :return: None
        '''
        #QToolButton()
        self.home_grid_Layout = QGridLayout()
        # self.home_button_1 = QToolButton()
        # self.home_button_1.setProperty('class', 'home')
        # self.home_button_1.setFixedSize(160,160)
        # self.home_button_1.setText("HomePage")
        # self.home_button_1.setIcon(QIcon(START_ICON))
        # self.home_button_1.setIconSize(QSize(60, 60))
        # self.home_button_1.setToolButtonStyle(Qt.ToolButtonTextUnderIcon)

        self.home_button_2 = QToolButton()
        self.home_button_2.setProperty('class', 'home')
        self.home_button_2.setFixedSize(160, 160)
        self.home_button_2.setText("Moodle")
        self.home_button_2.setIcon(QIcon(START_ICON))
        self.home_button_2.setIconSize(QSize(60, 60))
        self.home_button_2.setToolButtonStyle(Qt.ToolButtonTextUnderIcon)

        self.home_button_3 = QToolButton()
        self.home_button_3.setProperty('class', 'home')
        self.home_button_3.setFixedSize(160, 160)
        self.home_button_3.setText("Email")
        self.home_button_3.setIcon(QIcon(START_ICON))
        self.home_button_3.setIconSize(QSize(60, 60))
        self.home_button_3.setToolButtonStyle(Qt.ToolButtonTextUnderIcon)

        self.home_button_4 = QToolButton()
        self.home_button_4.setProperty('class', 'home')
        self.home_button_4.setFixedSize(160, 160)
        self.home_button_4.setText("Setting")
        self.home_button_4.setIcon(QIcon(START_ICON))
        self.home_button_4.setIconSize(QSize(60, 60))
        self.home_button_4.setToolButtonStyle(Qt.ToolButtonTextUnderIcon)

        # self.home_grid_Layout.addWidget(self.home_button_1, 0, 0)
        self.home_grid_Layout.addWidget(self.home_button_2, 0, 0)
        self.home_grid_Layout.addWidget(self.home_button_3, 0, 1)
        self.home_grid_Layout.addWidget(self.home_button_4, 0, 2)
        self.scrollArea.setLayout(self.home_grid_Layout)
        self.setStyleSheet(SYS_STYLE)

        self.home_button_2.clicked.connect(self.Moodle)
        self.home_button_3.clicked.connect(self.Email)
        self.home_button_4.clicked.connect(self.Setting)

    # Add function for button
    def Moodle(self):
        '''
        Login moodle, if not login, create login window
        :return: None
        '''
        # Determine whether a user is currently logged in
        if self.isuserlogin:
            print("User is login, open moodle page")
            self.home_login_signal.emit(self.username, self.userpassword, self.isuserlogin)
            # self.moodle_login.login_signal.connect(self.home_login_info)
            # self.moodle_window = MoodleWindow(self.username, self.userpassword)
            # self.stackedWidget.addWidget(self.moodle_window)
            # self.stackedWidget.setCurrentIndex(2)
        else:
            print("User is not login, open login page")
            self.moodle_login = LoginWindow()
            self.moodle_login.setWindowFlags(Qt.WindowStaysOnTopHint)
            self.moodle_login.show()
            self.moodle_login.login_signal.connect(self.home_login_info)

    def home_login_info(self, username, userpassword, isuserlogin):
        '''
        Set moodle information
        :param username: moodle username
        :param userpassword: moodle passward
        :param isuserlogin: whether login
        :return: None
        '''
        if isuserlogin:
            # userinfo
            self.isuserlogin = isuserlogin
            self.username = username
            self.userpassword = userpassword
            # Add Moodle interface index=2
            self.home_login_signal.emit(username, userpassword, isuserlogin)
        else:
            print("pass is error")
            self.home_login_signal.emit("", "", False)

    def Email(self):
        '''
        Connect email
        :return: None
        '''
        print("Email")
        # self.stackedWidget.setCurrentIndex(self.listWidget.currentRow())
        self.home_email_signal.emit('get Email')

    def Setting(self):
        '''
        Connect setting window
        :return: None
        '''
        print("Setting")
        # self.stackedWidget.setCurrentIndex(self.listWidget.currentRow())
        self.home_setting_signal.emit()


