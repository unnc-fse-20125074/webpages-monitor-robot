'''
author: Team 16
'''

import sys
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *


"""
@desc: This is the design of UI for pop window
"""


# This is pop window
class Pop_Window(QWidget):
    '''
    This is the design of UI for pop window
    '''
    def __init__(self, lst, parent=None):
        '''
        Init the pop window
        :param lst: Message want to pop
        :param parent: None
        '''
        super(Pop_Window, self).__init__(parent)
        self.init_ui(lst)
        # self.init_slot()

    def init_ui(self, lst):
        '''
        Add all messages want to pop
        :param lst: Message want to pop
        :return: None
        '''
        self.resize(400, 400)
        self.setWindowTitle('pop up')
        self.email_groupbox_layout = QVBoxLayout()
        self.email_groupbox_layout.setAlignment(Qt.AlignTop)
        for item in lst:
            for element in item:
                self.label = QLabel(element)
                self.email_groupbox_layout.addWidget(self.label)
        self.setLayout(self.email_groupbox_layout)


