'''
author: Team 16
'''

"""
coding:utf-8
file: frozen_dir.py
@desc:
"""
import sys
import os


def app_path():
    if hasattr(sys, 'frozen'):
        return os.path.dirname(sys.executable)
    return os.path.dirname(__file__)
