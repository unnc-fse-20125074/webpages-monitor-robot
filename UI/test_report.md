# Test Report

### Class: Stage 1 Moodle

<escape>
    <table border="1">
        <tr>
            <th>Project</th>
            <td colspan="8">Webpages monitor robot</td>
        </tr>
        <tr>
            <th>Class</th>
            <td colspan="8">Moodle</td>
        </tr>
        <tr>
            <th colspan="3">Test items</th>
            <th colspan="2">Develop information</th>
            <th colspan="4">Test process</th>
        </tr>
        <tr>
            <th>Number</th>
            <th>Function</th>
            <th>Level</th>
            <th>Tester</th>
            <th>Date</th>
            <th>Inputs/Operation</th>
            <th>Expected Outcome</th>
            <th>Test Outcome</th>
            <th>If pass</th>
        </tr>
        <tr>
            <th>1</th>
            <td>login</td>
            <td>&1&</td>
            <td>Zibo Zhao</td>
            <td>2021.03.16</td>
            <td><code>scyzz5<br/>Lxygwqfqsgct1s-</code></td>
            <td><code>True</code></td>
            <td><code>True</code></td>
            <td>Pass</td>
        </tr>
        <tr>
            <th>2</th>
            <td>login</td>
            <td>&1&</td>
            <td>Zibo Zhao</td>
            <td>2021.03.16</td>
            <td><code>scyzz5<br/>wrong_password</code></td>
            <td><code>False</code></td>
            <td><code>False</code></td>
            <td>Pass</td>
        </tr>
        <tr>
            <th>3</th>
            <td>test_connect</td>
            <td>&1&</td>
            <td>Zibo Zhao</td>
            <td>2021.03.16</td>
            <td><code>None</code></td>
            <td><code>False</code></td>
            <td><code>False</code></td>
            <td>Pass</td>
        </tr>
        <tr>
            <th>4</th>
            <td>test_connect</td>
            <td>&1&</td>
            <td>Zibo Zhao</td>
            <td>2021.03.16</td>
            <td><code>moodle.login("scyzz5", "Lxygwqfqsgct1s-")</code></td>
            <td><code>True</code></td>
            <td><code>True</code></td>
            <td>Pass</td>
        </tr>
        <tr>
            <th>5</th>
            <td>re_login</td>
            <td>&1&</td>
            <td>Zibo Zhao</td>
            <td>2021.03.16</td>
            <td><code>None</code></td>
            <td><code>False</code></td>
            <td><code>False</code></td>
            <td>Pass</td>
        </tr>
        <tr>
            <th>6</th>
            <td>logout</td>
            <td>&1&</td>
            <td>Zibo Zhao</td>
            <td>2021.03.16</td>
            <td><code>None</code></td>
            <td><code>False</code></td>
            <td><code>False</code></td>
            <td>Pass</td>
        </tr>
        <tr>
            <th>7</th>
            <td>logout</td>
            <td>&1&</td>
            <td>Zibo Zhao</td>
            <td>2021.03.16</td>
            <td><code>Moodle.login("scyzz5", "Lxygwqfqsgct1s-")</code></td>
            <td><code>True</code></td>
            <td><code>True</code></td>
            <td>Pass</td>
        </tr>
    </table>
</escape>

### Class: Stage 1 Url_json_list

<escape>
    <table border="1">
        <tr>
            <th>Project</th>
            <td colspan="8">Webpages monitor robot</td>
        </tr>
        <tr>
            <th>Class</th>
            <td colspan="8">Url_json_list</td>
        </tr>
        <tr>
            <th colspan="3">Test items</th>
            <th colspan="2">Develop information</th>
            <th colspan="4">Test process</th>
        </tr>
        <tr>
            <th>Number</th>
            <th>Function</th>
            <th>Level</th>
            <th>Tester</th>
            <th>Date</th>
            <th>Inputs/Operation</th>
            <th>Expected Outcome</th>
            <th>Test Outcome</th>
            <th>If pass</th>
        </tr>
        <tr>
            <th>1</th>
            <td>login_request_url</td>
            <td>&1&</td>
            <td>Zibo Zhao</td>
            <td>2021.03.16</td>
            <td><code>None</code></td>
            <td><code>"https://moodle.nottingham.ac.uk/login/index.php"</code></td>
            <td><code>"https://moodle.nottingham.ac.uk/login/index.php"</code></td>
            <td>Pass</td>
        </tr>
        <tr>
            <th>2</th>
            <td>home_page_url</td>
            <td>&1&</td>
            <td>Zibo Zhao</td>
            <td>2021.03.16</td>
            <td><code>None</code></td>
            <td><code>"https://moodle.nottingham.ac.uk/my/"</code></td>
            <td><code>"https://moodle.nottingham.ac.uk/my/"</code></td>
            <td>Pass</td>
        </tr>
        <tr>
            <th>3</th>
            <td>log_out_request_url</td>
            <td>&1&</td>
            <td>Zibo Zhao</td>
            <td>2021.03.16</td>
            <td><code>None</code></td>
            <td><code>"https://moodle.nottingham.ac.uk/login/logout.php?sesskey="</code></td>
            <td><code>"https://moodle.nottingham.ac.uk/login/logout.php?sesskey="</code></td>
            <td>Pass</td>
        </tr>
        <tr>
            <th>4</th>
            <td>folder_download_request_url</td>
            <td>&1&</td>
            <td>Zibo Zhao</td>
            <td>2021.03.16</td>
            <td><code>None</code></td>
            <td><code>"https://moodle.nottingham.ac.uk/mod/folder/download_folder.php"</code></td>
            <td><code>"https://moodle.nottingham.ac.uk/mod/folder/download_folder.php"</code></td>
            <td>Pass</td>
        </tr>
        <tr>
            <th>5</th>
            <td>course_list_request_url_1</td>
            <td>&1&</td>
            <td>Zibo Zhao</td>
            <td>2021.03.16</td>
            <td><code>None</code></td>
            <td><code>"https://moodle.nottingham.ac.uk/lib/ajax/service.php?sesskey="</code></td>
            <td><code>"https://moodle.nottingham.ac.uk/lib/ajax/service.php?sesskey="</code></td>
            <td>Pass</td>
        </tr>
        <tr>
            <th>6</th>
            <td>course_list_request_url_2</td>
            <td>&1&</td>
            <td>Zibo Zhao</td>
            <td>2021.03.16</td>
            <td><code>None</code></td>
            <td><code>"&info=core_course_get_enrolled_courses_by_timeline_classification"</code></td>
            <td><code>"&info=core_course_get_enrolled_courses_by_timeline_classification"</code></td>
            <td>Pass</td>
        </tr>
        <tr>
            <th>7</th>
            <td>all_course_list_json</td>
            <td>&1&</td>
            <td>Zibo Zhao</td>
            <td>2021.03.16</td>
            <td><code>None</code></td>
            <td><code>$json_code$</code></td>
            <td><code>$json_code$</code></td>
            <td>Pass</td>
        </tr>
        <tr>
            <th>7</th>
            <td>stared_course_list_json</td>
            <td>&1&</td>
            <td>Zibo Zhao</td>
            <td>2021.03.16</td>
            <td><code>None</code></td>
            <td><code>$json_code$</code></td>
            <td><code>$json_code$</code></td>
            <td>Pass</td>
        </tr>
    </table>
</escape>

### Class: Stage 1 FileForMac

<escape>
    <table border="1">
        <tr>
            <th>Project</th>
            <td colspan="8">Webpages monitor robot</td>
        </tr>
        <tr>
            <th>Class</th>
            <td colspan="8">FileForMac</td>
        </tr>
        <tr>
            <th colspan="3">Test items</th>
            <th colspan="2">Develop information</th>
            <th colspan="4">Test process</th>
        </tr>
        <tr>
            <th>Number</th>
            <th>Function</th>
            <th>Level</th>
            <th>Tester</th>
            <th>Date</th>
            <th>Inputs/Operation</th>
            <th>Expected Outcome</th>
            <th>Test Outcome</th>
            <th>If pass</th>
        </tr>
        <tr>
            <th>1</th>
            <td>load_json</td>
            <td>&1&</td>
            <td>Zibo Zhao</td>
            <td>2021.03.16</td>
            <td><code>None</code></td>
            <td><code>False</code></td>
            <td><code>False</code></td>
            <td>Pass</td>
        </tr>
        <tr>
            <th>2</th>
            <td>load_json</td>
            <td>&1&</td>
            <td>Zibo Zhao</td>
            <td>2021.03.16</td>
            <td><code>&PATH OF JSON&</code></td>
            <td><code>&INFO&</code></td>
            <td><code>&INFO&</code></td>
            <td>Pass</td>
        </tr>
        <tr>
            <th>3</th>
            <td>saveJson</td>
            <td>&1&</td>
            <td>Zibo Zhao</td>
            <td>2021.03.16</td>
            <td><code>None</code></td>
            <td><code>False</code></td>
            <td><code>False</code></td>
            <td>Pass</td>
        </tr>
        <tr>
            <th>4</th>
            <td>saveJson</td>
            <td>&1&</td>
            <td>Zibo Zhao</td>
            <td>2021.03.16</td>
            <td><code>&PATH&, &DATA&, &NAME&</code></td>
            <td><code>FILE EXIST</code></td>
            <td><code>FILE EXIST</code></td>
            <td>Pass</td>
        </tr>
    </table>
</escape>

### Class: Stage 1 File

<escape>
    <table border="1">
        <tr>
            <th>Project</th>
            <td colspan="8">Webpages monitor robot</td>
        </tr>
        <tr>
            <th>Class</th>
            <td colspan="8">File</td>
        </tr>
        <tr>
            <th colspan="3">Test items</th>
            <th colspan="2">Develop information</th>
            <th colspan="4">Test process</th>
        </tr>
        <tr>
            <th>Number</th>
            <th>Function</th>
            <th>Level</th>
            <th>Tester</th>
            <th>Date</th>
            <th>Inputs/Operation</th>
            <th>Expected Outcome</th>
            <th>Test Outcome</th>
            <th>If pass</th>
        </tr>
        <tr>
            <th>1</th>
            <td>load_json</td>
            <td>&1&</td>
            <td>Zibo Zhao</td>
            <td>2021.03.16</td>
            <td><code>None</code></td>
            <td><code>False</code></td>
            <td><code>False</code></td>
            <td>Pass</td>
        </tr>
        <tr>
            <th>2</th>
            <td>load_json</td>
            <td>&1&</td>
            <td>Zibo Zhao</td>
            <td>2021.03.16</td>
            <td><code>&PATH OF JSON&</code></td>
            <td><code>&INFO&</code></td>
            <td><code>&INFO&</code></td>
            <td>Pass</td>
        </tr>
        <tr>
            <th>3</th>
            <td>saveJson</td>
            <td>&1&</td>
            <td>Zibo Zhao</td>
            <td>2021.03.16</td>
            <td><code>None</code></td>
            <td><code>False</code></td>
            <td><code>False</code></td>
            <td>Pass</td>
        </tr>
        <tr>
            <th>4</th>
            <td>saveJson</td>
            <td>&1&</td>
            <td>Zibo Zhao</td>
            <td>2021.03.16</td>
            <td><code>&PATH&, &DATA&, &NAME&</code></td>
            <td><code>FILE EXIST</code></td>
            <td><code>FILE EXIST</code></td>
            <td>Pass</td>
        </tr>
    </table>
</escape>

### Class: Stage 2 MoodleCrawler

<table>
<thead>
  <tr>
    <th rowspan="6"><code>@pytest.fixture(scope="class")</code> Structure Moodle</th>
    <th>Operation</th>
    <th>Code</th>
    <th>Return</th>
  </tr>
  <tr>
    <td rowspan="5">Structure <code>Moodle</code> object and <code>login().</code></td>
    <td rowspan="5"><code>moodle = Moodle()<br/>moodle.login("scyzz5", "Lxygwqfqsgct1s-")</code></td>
    <td rowspan="5">True</td>
  </tr>
</thead>
<thead>
  <tr>
    <th rowspan="6"><code>@pytest.fixture(scope="class")</code> Structure MoodleCrawler</th>
    <th>Operation</th>
    <th>Code</th>
    <th>Return</th>
  </tr>
  <tr>
    <td rowspan="5">Structure <code>MoodleCrawler</code> object and input <code>Moodle</code> object as a parameter</td>
    <td rowspan="5"><code>crawler = MoodleCrawler(moodle)</code></td>
    <td rowspan="5"><code>MoodleCrawler</code></td>
  </tr>
</thead>
</table>

<table>
<thead>
  <tr>
    <th colspan="4">Test items</th>
    <th colspan="5">Test process</th>
    <th></th>
  </tr>
</thead>
<tbody>
  <tr>
    <td>Number</td>
    <td>Function</td>
    <td>Test Number</td>
    <td>Test Description </td>
    <td>Operation</td>
    <td>Inputs</td>
    <td>Expected Outcome</td>
    <td>Test Outcome</td>
    <td>If pass</td>
    <td>Level</td>
  </tr>
  <tr>
    <td>1</td>
    <td>1) getList </td>
    <td>1.1)</td>
    <td>Get modules list </td>
    <td>Call <code>Crawler.getList()</code></td>
    <td>True</td>
    <td>All modules in Moodle with module_name, module_url, module_id, monitoring</td>
    <td><code>[{"course_name": "Academic Services Office (CN)", "course_url": "https://moodle.nottingham.ac.uk/course/view.php?id=10822", "monitoring": false, "id": 0}<br/> .. ]</code></td>
    <td>Pass</td>
    <td>2</td>
  </tr>
  <tr>
    <td>1</td>
    <td>1) getList </td>
    <td>1.2)</td>
    <td>Get modules list </td>
    <td>Call <code>Crawler.getList()</code></td>
    <td>False</td>
    <td>Stared modules in Moodle with module_name, module_url, module_id, monitoring</td>
    <td><code>[{"course_name": "Algorithms Correctness and Efficiency (COMP2048 UNNC) (FCH1 20-21)", "course_url": "https://moodle.nottingham.ac.uk/course/view.php?id=104761", "monitoring": true, "id": 1} <br/> .. ]</code></td>
    <td>Pass</td>
    <td>2</td>
  </tr>
</tbody>
</table>

