# README 

#### Project Name: Webpages Monitor Robot 

#### Develop Team: Team 16



## Contributors

- Xi YU (20123805)
- Jiayin WANG (20126336)
- Zhekai HAN (20125074)
- Zibo ZHAO (20126911)
- Zihan LIU (16521717)



## Environment Requirements

### For running executive software:

- **Required OS:**

  **-**  Windows

  **-**  macOS(still in the development stage)

### For running source code:

- **Required OS: **

  **-**  Windows

  **-**  macOS(still in the development stage)

- **Required Python version:**

  **-** Python 3.9

- **Required library:** （All libraries are also written in [required libraries](requirements.txt)）

  | Library        | Version   |
  | -------------- | --------- |
  | altgraph       | 0.17      |
  | APScheduler    | 3.7.0     |
  | beautifulsoup4 | 4.9.3     |
  | bs4            | 0.0.1     |
  | certifi        | 2020.12.5 |
  | chardet        | 4.0.0     |
  | click          | 7.1.2     |
  | idna           | 2.10      |
  | lxml           | 4.6.3     |
  | macholib       | 1.14      |
  | modulegraph    | 0.18      |
  | pip-bundle     | 0.0.10    |
  | PyQt5          | 5.15.4    |
  | PyQt5-Qt5      | 5.15.2    |
  | PyQt5-sip      | 12.8.1    |
  | PyQt5-stubs    | 5.15.2.0  |
  | pytz           | 2021.1    |
  | PyYAML         | 5.4.1     |
  | requests       | 2.25.1    |
  | six            | 1.15.0    |
  | soupsieve      | 2.2.1     |
  | tzlocal        | 2.1       |
  | urllib3        | 1.26.4    |
  | zmail          | 0.2.8     |

## **Installation Instruction**

### Two ways to run the program:

#### Open executable software:

We have packaged the software for users. You can go to download the entire executable software: https://gitlab.com/unnc-fse-20125074/wmr-public

After download, open

<u>**For Windows users:**</u> wmr-public/Windows/dist/main_window

(<u>**For macOS users**:</u> wmr-public/macOS(alpha)/dist/main_window )

Open the software by clicking main_window



#### Run source code:

Download the source code from https://gitlab.com/unnc-fse-20125074/webpages-monitor-robot and unzip the file.

Make sure you have the Python 3.9 environment.

You can choose to run either in terminal or IDE that supports Python (like PyCharm).



##### Run in terminal:

- Install required libraries:

  Open terminal and go into the webpages-monitor-robot

  ```
  // For example
  cd webpages-monitor-robot-master
  ```

  Install all required libraries directly by input

  ```
  pip3 install -r requirements.txt
  ```

- Run main_window.py

  ```
  // For Windows
  python main_window.py
  // For macOS
  python3 main_window.py
  ```

- And then, the UI will be shown




##### Run in IDE:

- Open webpages-monitor-robot as project in IDE

- Install all required libraries manually or open the terminal in the IDE and input:

  ```
  pip3 install -r requirements.txt
  ```

- Run main_window.py



## Quality Assurance

- [Quality Assurance](Quality Assurance/Quality Assurance.pdf)



## User Manual

- [User Manual](User Manual/User Manual.pdf)

